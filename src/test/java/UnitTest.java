import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quiputech.security.module.RedHatManager;
import static com.quiputech.security.module.RedHatManager.getUsers;
import com.quiputech.security.module.entities.AccessTokenResponse;
import com.quiputech.security.module.entities.Attributes;
import com.quiputech.security.module.entities.BaseRequest;
import com.quiputech.security.module.entities.UserFilterRequest;
import com.quiputech.security.module.entities.UserRepresentation;
import com.quiputech.security.module.entities.UserRequest;
import static com.quiputech.security.module.utils.Constants.GET_TOKEN;
import com.quiputech.security.module.utils.Util;
import java.io.IOException;
import java.util.Arrays;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;
import javax.ws.rs.core.MediaType;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.quiputech.security.module.CognitoManager;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author mgaldamez
 */
public class UnitTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UnitTest.class);
     private static final ObjectMapper MAPPER = new ObjectMapper();
    
    @org.testng.annotations.Test(enabled = false)
    public void test() {
        CognitoManager.CLIENTID = "4384q37beicogd4hr9d3e9ue0f";
        CognitoManager.USERPOOLID = "us-east-2_mlBKuFDHk";
        CognitoManager.REGION = "US-EAST-2";
        Map<String, String> att = new LinkedHashMap<>();

        att = CognitoManager.getUserAttributes("mgaldamez");
        
        String tesf = att.get("middle_name");
        
        String json = "[{\"id\":\"5986f67e-323c-414e-a723-a2d3e8aa9d06\",\"createdTimestamp\":1587447621109,\"username\":\"sfernandez\",\"enabled\":true,\"totp\":false,\"emailVerified\":true,\"firstName\":\"Steven\",\"lastName\":\"Fernandez\",\"email\":\"michael@quiputech.com\",\"attributes\":{\"test\":[\"7325c1de-b05b-4f84-b321-9adc6e61f4a2\"],\"state\":[\"activo\"]},\"disableableCredentialTypes\":[\"password\"],\"requiredActions\":[\"UPDATE_PASSWORD\"],\"notBefore\":0,\"access\":{\"manageGroupMembership\":true,\"view\":true,\"mapRoles\":true,\"impersonate\":true,\"manage\":true}}]";
        String accessToken = "";
        String getToken = "{\n" +
                            "	\"grant_type\": \"password\",\n" +
                            "	\"client_id\": \"admin-cli\",\n" +
                            "	\"username\": \"mgaldamez\",\n" +
                            "	\"password\": \"Av3$truz\"\n" +
                            "}";
        
        String userRequest = "{\n" +
                            "	\"username\": \"sfernandez\",\n" +
                            "	\"firstName\": \"Steven\",\n" +
                            "	\"lastName\": \"Fernandez\",\n" +
                            "	\"emailVerified\": true,\n" +
                            "	\"email\": \"sfernandez@niubiz.com.pe\",\n" +
                            "	\"enabled\": true,\n" +
                            "	\"credentials\": [\n" +
                            "		{\n" +
                            "			\"temporary\": false,\n" +
                            "			\"type\": \"password\",\n" +
                            "			\"value\": \"sfernandez\"\n" +
                            "		}\n" +
                            "	],\n" +
                            "   \"attributes\": [\"M3l4n1@.\", \"Hola\"],\n" +
                            "	\"requiredActions\": [\"UPDATE_PASSWORD\"]\n" +
                            "}";
        RedHatManager.REALM = "quipuTech";
        RedHatManager.USERNAME = "mgaldamez";
        RedHatManager.PASSWORD = "Av3$truz";
        //String response = RedHatManager.getAccessTokenString(getToken);
        Boolean created = false, deleted = false;
        HashMap<String, String> attributes = new HashMap<>();
        //attributes.put("email", "mgaldamez@quiputech.com");
        attributes.put("firstName", "Steven");
        try {
            //deleted = RedHatManager.deleteUser("sfernandez");
            //Gson gson = new GsonBuilder().setPrettyPrinting().create();
            //UserRepresentation[] loco = gson.fromJson(json, UserRepresentation[].class);
            //UserRepresentation[] loco = MAPPER.readValue(json, UserRepresentation[].class);
            //List<UserRepresentation> list = Arrays.asList(MAPPER.readValue(json, UserRepresentation[].class));
            //List<UserRepresentation> pp3 = MAPPER.readValue(json, new TypeReference<UserRepresentation[]>() {});
            //List<UserRepresentation> resp = getUsers(attributes);
            //created = RedHatManager.createUser(MAPPER.readValue(userRequest, UserRequest.class));
            //System.out.print(loco);
            CognitoManager.CLIENTID = "5i7amsbclgija36tv17oufm0sh";
            CognitoManager.USERPOOLID = "us-east-2_LfCbduwsG";
            CognitoManager.REGION = "US-EAST-2";
            UserRequest user = new UserRequest();
            user.setEmail("micahs@123.com");
            user.setName("Test1");
            user.setGiven_name("Test1");
            user.setZoneinfo("12");
            user.setProfile("Acq");
            user.setUsername("test123");
            user.setFamily_name("Test1");
            HashMap<String, String> attribute1s = new HashMap<>();
            attribute1s.put("state", "MTest");
            attribute1s.put("entity", "1234512412");
            user.setAttributes(attribute1s);
            CognitoManager.setUserAttributes(user);
            created = CognitoManager.createCognitoAccount(user);
        } catch (Exception ex) {
            java.util.logging.Logger.getLogger(UnitTest.class.getName()).log(Level.SEVERE, null, ex);
        }

        //System.out.print(response);
        System.out.print("HASHMAP!!!! " + created.toString());
    }
    /*
    @org.testng.annotations.Test(enabled = false)
    public void testRedhat() {
        //String text = RedHatManager.getAccessTokenString();
        //String text1 = RedHatManager.getAccessTokenString("mgaldamez", "Av3$truz");
        //String text2 = RedHatManager.getRealmCertsUrl();
        //String text3 = RedHatManager.getRealmUrl();
        //AccessToken text4 = RedHatManager.getAccessToken();
        //AccessToken text5 = RedHatManager.getAccessToken("mgaldamez", "Av3truz");
        UserRepresentation user = new UserRepresentation();
        user.setEnabled(true);
        user.setUsername("");
        user.setFirstName("");
        user.setLastName("");
        user.setEmail("");
        user.setEmailVerified(true);
        //user.setCredentials(Arrays.asList(passwordCred));
        RedHatManager.createUser(user);
        RedHatManager.addUserToRole("test1", "tester");
        RedHatManager.updateUserPassword("test1", "michael");
        RedHatManager.deleteUser("test1");
    }  */ 
    
    @org.testng.annotations.Test(enabled = true)
    public void testDB() {
        RedHatManager.REALM = "posservicios";
        RedHatManager.USERNAME = "admin";
        RedHatManager.PASSWORD = "admin";
        
        RedHatManager.createToken(RedHatManager.USERNAME, RedHatManager.PASSWORD);
        
    }   
}


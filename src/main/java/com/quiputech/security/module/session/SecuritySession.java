/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.session;

import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.crypto.CryptoUtil;
import com.quiputech.security.module.crypto.SaltedSecretKey;
import com.quiputech.security.module.enums.AuthorizeType;
import com.quiputech.security.module.utils.ServiceLocator;
import com.quiputech.security.module.dto.AuthInformation;
import com.quiputech.security.module.dto.SecurityKey;
import com.quiputech.security.module.enums.KeyType;
import com.quiputech.security.module.rsa.RSAPairKeys;
import com.quiputech.security.module.utils.SecurityUtil;
import com.quiputech.security.module.utils.StringUtil;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Peter
 */
public class SecuritySession {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecuritySession.class);

    public List<AuthInformation> retrieveAuthInformation(String userName, String path) {

        Connection cnx = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        List<AuthInformation> list = null;

        try {
            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("SELECT w.* \n"
                    + "FROM ecorev3.APP_AUTH_WHITELIST w \n"
                    + "WHERE w.userName = ? AND w.status > 0 \n"
                    + "AND '" + path + "' REGEXP w.path");

            stmt.setString(1, userName);

            //
            rs = stmt.executeQuery();

            if (rs != null) {
                list = new ArrayList<>();

                while (rs.next()) {
                    AuthInformation dto = new AuthInformation();
                    dto.setActive(rs.getInt("status") > 0);
                    dto.setAppId(rs.getString("appId"));
                    dto.setPath(rs.getString("path"));
                    dto.setPathIdentifier(rs.getInt("usePathIdentifier") > 0);
                    dto.setIdentifierList(rs.getString("identifierList"));
                    dto.setIdentifierPathPosition(rs.getInt("identifierPathPosition"));
                    dto.setRemoteIps(rs.getString("remoteIps"));
                    dto.setUserName(rs.getString("userName"));
                    dto.setAuthorizeType(AuthorizeType.parse(rs.getString("authorize")));
                    dto.setMerchantPosition(rs.getInt("merchantPosition"));
                    dto.setValidateMerchant(rs.getInt("validateMerchant") > 0);
                    list.add(dto);
                }
            }

        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return list;
    }

    public boolean isMerchantValid(String userName, String merchantId) {

        Connection cnx = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        boolean result = false;

        try {
            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("SELECT count(0) AS RESULT from backofficevn.bo_commerce_group g, "
                    + "backofficevn.bo_commerce c "
                    + "where g.email = ? AND c.commerce_code = ? "
                    + "AND g.id = c.commerce_group "
                    + "AND g.state = 'Activo' "
                    + "AND c.state = 'Activo'");

            stmt.setString(1, userName);
            stmt.setString(2, merchantId);

            //
            rs = stmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    result = rs.getInt("RESULT") > 0;
                }
            }

        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }

    public boolean isIdempotent(String accessToken, String endpoint, String payload) {

        Connection cnx = null;
        PreparedStatement stmt = null;
        int rs = 0;
        boolean result = false;
        String uuid = UUID.randomUUID().toString();

        try {
            boolean isRegistrable = endpoint != null && !endpoint.trim().isEmpty();
            isRegistrable = isRegistrable && payload != null && !payload.trim().isEmpty();

            if (!isRegistrable) {
                return !isRegistrable;
            }

            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("INSERT INTO ecorev3.APP_IDEMPOTENT_EXECUTION (requestId, accessToken, endpointHash, payloadHash) \n"
                    + "SELECT ?, ?, ?, ? FROM DUAL \n"
                    + "WHERE NOT EXISTS \n"
                    + "(SELECT null FROM ecorev3.APP_IDEMPOTENT_EXECUTION WHERE endpointHash = ? AND payloadHash = ?)");

            String endpointHash = SecurityUtil.sha256(endpoint);
            String payloadHash = SecurityUtil.sha256(payload);

            stmt.setString(1, uuid);
            stmt.setString(2, accessToken != null && !accessToken.isEmpty() ? accessToken : SecurityConstants.NOT_PROVIDED);
            stmt.setString(3, endpointHash);
            stmt.setString(4, payloadHash);
            stmt.setString(5, endpointHash);
            stmt.setString(6, payloadHash);

            //
            rs = stmt.executeUpdate();

            result = rs > 0;

        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }
    
    public boolean addApiLog(String correlationId, int httpStatus, String path, long startDate, String user, String clientIp, String userAgent, String payload, String response) {

        Connection cnx = null;
        PreparedStatement stmt = null;
        int rs = 0;
        boolean result = false;

        try {
            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("INSERT INTO ecorev3.LOG_API_EXECUTION \n" +
                                        "(correlationId,\n" +
                                        "path,\n" +
                                        "httpStatus,\n" +
                                        "userName,\n" +
                                        "clientIp,\n" +
                                        "userAgent,\n" +
                                        "millis,\n" +
                                        "request,\n" +
                                        "response)\n" +
                                        "VALUES\n" +
                                        "(?, ?, ?, ?, ?, ?, ?, ?, ?)");
            stmt.setString(1, correlationId);
            stmt.setString(2, path);
            stmt.setInt(3, httpStatus);
            stmt.setString(4, user);
            stmt.setString(5, clientIp);
            stmt.setString(6, userAgent);
            long endDate = Calendar.getInstance().getTimeInMillis();
            stmt.setInt(7, (int) (endDate - startDate));
            stmt.setString(8, payload);
            stmt.setString(9, response);

            //
            rs = stmt.executeUpdate();

            result = rs > 0;

        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }

    public boolean createKeys(String tokenId, String user, SecurityKey exposed, SecurityKey wrapped) {

        Connection cnx = null;
        PreparedStatement stmt = null;
        boolean result = false;
        int rs = 0;

        try {
            if (exposed != null && wrapped != null) {
                DataSource ds = ServiceLocator.getInstance().getDataSource();
                cnx = ds.getConnection();

                stmt = cnx.prepareStatement("INSERT INTO ecorev3.SECURITY_KEY_STORE (tokenId, user, exposedToken, exposedBase, exposedIv, exposedSalt, wrappedToken, wrappedBase, wrappedIv, wrappedSalt) "
                        + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?) "
                        + "ON DUPLICATE KEY UPDATE user = ?, "
                        + "exposedToken = ?, exposedBase = ?,"
                        + "exposedIv = ?, exposedSalt = ?,"
                        + "wrappedToken = ?, wrappedBase = ?, "
                        + "wrappedIv = ?, wrappedSalt = ?");

                stmt.setString(1, tokenId);
                stmt.setString(2, user);
                stmt.setString(3, exposed.getToken());
                stmt.setString(4, exposed.getBaseKey());
                stmt.setString(5, exposed.getIv());
                stmt.setString(6, exposed.getSalt());
                stmt.setString(7, wrapped.getToken());
                stmt.setString(8, wrapped.getBaseKey());
                stmt.setString(9, wrapped.getIv());
                stmt.setString(10, wrapped.getSalt());
                stmt.setString(11, user);
                stmt.setString(12, exposed.getToken());
                stmt.setString(13, exposed.getBaseKey());
                stmt.setString(14, exposed.getIv());
                stmt.setString(15, exposed.getSalt());
                stmt.setString(16, wrapped.getToken());
                stmt.setString(17, wrapped.getBaseKey());
                stmt.setString(18, wrapped.getIv());
                stmt.setString(19, wrapped.getSalt());

                //
                rs = stmt.executeUpdate();
            }

            result = rs > 0;
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }
    
    public RSAPairKeys retrieveRSAKeys(String tokenId) {
       Connection cnx = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        RSAPairKeys result = null;

        try {
            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("SELECT * from ecorev3.RSA_KEYS where tokenId = ? ");

            stmt.setString(1, tokenId);

            //
            rs = stmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    try {
                        String publicKey = rs.getString("publicKey");
                        String privateKey = rs.getString("privateKey");
                        
                        result = new RSAPairKeys(publicKey, privateKey);
                    } catch (Exception ex) {
                        LOGGER.error(ex.getLocalizedMessage(), ex);
                    }
                }
            }

        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }

    public Map<String, SecurityKey> retrieveKeys(String tokenId) {
        Connection cnx = null;
        PreparedStatement stmt = null;
        ResultSet rs = null;
        Map<String, SecurityKey> result = null;

        try {
            DataSource ds = ServiceLocator.getInstance().getDataSource();
            cnx = ds.getConnection();

            stmt = cnx.prepareStatement("SELECT * from ecorev3.SECURITY_KEY_STORE "
                    + "where tokenId = ? ");

            stmt.setString(1, tokenId);

            //
            rs = stmt.executeQuery();

            if (rs != null) {
                while (rs.next()) {
                    try {
                        String exposedToken = rs.getString("exposedToken");
                        String exposedBase = rs.getString("exposedBase");
                        String exposedIv = rs.getString("exposedIv");
                        String exposedSalt = rs.getString("exposedSalt");

                        String wrappedToken = rs.getString("wrappedToken");
                        String wrappedBase = rs.getString("wrappedBase");
                        String wrappedIv = rs.getString("wrappedIv");
                        String wrappedSalt = rs.getString("wrappedSalt");

                        SaltedSecretKey exposedDerived = CryptoUtil.createDerivedKey(exposedBase, exposedSalt);
                        SaltedSecretKey wrapperDerived = CryptoUtil.createDerivedKey(wrappedBase, wrappedSalt);

                        result = new HashMap<>();
                        result.put(exposedToken, new SecurityKey(exposedBase, StringUtil.toHex(exposedDerived.getEncoded()), exposedSalt, exposedIv, KeyType.EXPOSED));
                        result.put(wrappedToken, new SecurityKey(wrappedBase, StringUtil.toHex(wrapperDerived.getEncoded()), wrappedSalt, wrappedIv, KeyType.WRAPPED));
                    } catch (Exception ex) {
                        LOGGER.error(ex.getLocalizedMessage(), ex);
                    }
                }
            }

        } catch (SQLException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
            try {
                if (stmt != null) {
                    stmt.close();
                }
            } catch (SQLException sse) {
                LOGGER.error(sse.getLocalizedMessage(), sse);
            }
            if (cnx != null) {
                try {
                    if (!cnx.isClosed()) {
                        cnx.close();
                    }
                } catch (SQLException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
            }
        }

        return result;
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.constants;

/**
 *
 * @author Peter Galdamez
 */
public class SecurityConstants {
    public static final String COGNITO_IDENTITY_POOL_URL = "https://cognito-idp.%s.amazonaws.com/%s";
    public static final String JSON_WEB_TOKEN_SET_URL_SUFFIX = "/.well-known/jwks.json";
    //public static final String COGNITO_APP_CLIENT_ID = System.getProperty("api.security.client.id", "10lv0617o5dic51ebsnqeiijb7");
    public static final String AWS_ACCESS_KEY_ID = System.getProperty("api.security.aws.access.id", "AKIAJLHKCP4INCUJ67EA");
    public static final String AWS_SECRET_ACCESS_KEY = System.getProperty("api.security.aws.secret", "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");
    public static final String NO_AUTHORIZATION = "Unauthorized access";
    public static final String NOT_SECURE = "Unauthorized access";
    public static final String FORBIDDEN_ACCESS = "Forbidden access";
    public static final String NOT_ACCEPTABLE = "Too many request with same payload [Idempotent policy]";
    public static final String NOT_ALLOWED = "Request is not allowed";
    public static final String NOT_VALID = "Request is not valid";
    public static final int NO_AUTHORIZATION_CODE = 401;
    public static final int NO_ALLOWED_CODE = 403;
    public static final int NO_IDEMPOTENT_CODE = 406;
    public static final int BAD_REQUEST_CODE = 400;
    public static final int UNEXPECTED = 500;
    public static final String UNEXPECTED_ERROR = "Unexpected Error";
    public static final String BAD_REQUEST_MESSAGE = "Bad Request";
    public static final String OPTIONS = "OPTIONS";
    public static final String SECURITY_USER = "SECURITY_USER";
    public static final String SECURITY_REMOTE_IP = "SECURITY_REMOTE_IP";
    public static final String APPLICATION_JSON_CHARSET = "application/json; charset=utf-8";   
    public static final String NOT_PROVIDED = "[NOT PROVIDED]";
    public static final String SCOPE = "Scope";
    public static final String X_CORRELATION_ID = "X-Correlation-Id";
    public static final String X_TIME_START = "X-Time-Start";
    public static final String X_PAYLOAD_REQUEST = "X-Payload-Request";
    public static final String CRYPTO_CONTEXT_EWSALT = "VisaNet-Crypto-Context-ewSalt";
    public static final String CRYPTO_CONTEXT_EWIV = "VisaNet-Crypto-Context-ewIV";
    public static final String CRYPTO_CONTEXT_KEY = "VisaNet-Crypto-Context-Key";
    public static final String CORRELATION_ID = "correlationId";
    /*Main datasource */
    public static final String ECORE_DATASOURCE = System.getProperty("security-commons.datasource", "java:/ecorev3DS");
}

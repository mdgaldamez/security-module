package com.quiputech.security.module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.quiputech.security.module.dto.SecurityResponse;
import com.quiputech.security.module.entities.*;
import static com.quiputech.security.module.utils.Constants.*;
import com.quiputech.security.module.utils.Util;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Level;
import javax.ws.rs.core.MediaType;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdámez
 */
public class RedHatManager {
    private static final Logger LOGGER = LoggerFactory.getLogger(RedHatManager.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();
    public static String REALM;
    // idm-client needs to allow "Direct Access Grants: Resource Owner Password Credentials Grant"
    public static String CLIENT_ID;
    public static String CLIENT_SECRET;
    public static String USERNAME;
    public static String PASSWORD;
    public static String ACCESS_TOKEN;

    public static void AdminInitiate(){
        //Get Access Token
        String accessTokenRequest = TOKEN.replace("{username}", USERNAME);
        accessTokenRequest = accessTokenRequest.replace("{password}", PASSWORD);
        ACCESS_TOKEN = getAccessTokenString(accessTokenRequest);
    }
    
    public static SecurityResponse createToken(String userName, String password) {
        try {
            String accessTokenRequest = TOKEN.replace("{username}", userName);
            accessTokenRequest = accessTokenRequest.replace("{password}", password);
            AccessTokenResponse authResponse = getAccessTokenResponse(accessTokenRequest);
            
            List<String> roles = getRoles(userName);
            SecurityResponse response = SecurityResponse.builder(authResponse).build();
            response.setGroups(roles);
            return response;
        } catch (Exception e) {
             LOGGER.error("Unable to obtain access token.\n", e);
            return null;
        }
    }
    
    public static boolean createUser(UserRequest request) {
        // Create user (requires manage-users role)
        boolean result = false;
        if (request.getEmail() == null || request.getEmail().isEmpty()) {
            return result;
        }
        try 
        {
            AdminInitiate();
            
            // Define password credential
            CredentialRepresentation passwordCred = new CredentialRepresentation();
            
            List<CredentialRepresentation> cred = request.getCredentials();
            passwordCred.setValue(cred.get(0).getValue());
            passwordCred.setType(cred.get(0).getType());
            passwordCred.setTemporary(cred.get(0).isTemporary());
            
            // Define user
            UserRepresentation user = new UserRepresentation();
            user.setEnabled(Boolean.valueOf(request.getEnabled()));
            user.setUsername(request.getUsername());
            user.setFirstName(request.getName());
            user.setLastName(request.getFamily_name());
            user.setEmail(request.getEmail());
            user.setEmailVerified(Boolean.valueOf(request.getEmail_verified()));
            user.setCredentials(Arrays.asList(passwordCred));
            List<String> requiredActionList = new ArrayList<>();
            for (String action: request.getRequiredActions()){
                requiredActionList.add(action);
            }
            user.setRequiredActions(requiredActionList);
            if(request.getAttributes() != null) {
                Map<String, List<String>> attr = new HashMap<>();
                
                Iterator it = request.getAttributes().entrySet().iterator();
                while (it.hasNext()) {
                    List<String> list = new ArrayList<>();
                    Map.Entry pair = (Map.Entry)it.next(); 
                    list.add(pair.getValue().toString());
                    attr.put(pair.getKey().toString(), list);
                    it.remove(); // avoids a ConcurrentModificationException
                }

                user.setAttributes(attr);
            }
            
            String URL = USERS.replace("{realm}", REALM);
            String response = Util.executePOSTRequest(MAPPER.writeValueAsString(user), URL, ACCESS_TOKEN);
            
            if (!response.isEmpty()) {
                LOGGER.error(String.format("CREATE USER: ERROR: Unable to create the user\n%s", response));
            } else {
                result = true;
            }
        }
        catch(Exception ex)
        {
            LOGGER.error("ERROR --> "  + ex.getMessage());
        }
        return result;
    }
    
    public static boolean updateUser(UserRequest request) {
        boolean result = false;
        try {
            AdminInitiate();

            // Get User
            HashMap<String, String> attributes = new HashMap<>();
            attributes.put("username", request.getUsername());
            List<UserRepresentation> users = getUsers(attributes);
            if(users.size() != 1)
            {
                LOGGER.debug("Skipping user update! Found " + users.size() + " users while trying to update, expected 1");
            }
            boolean updated = false;
            UserRepresentation user = users.get(0);
            if (request.getEmail() != null && !request.getEmail().trim().isEmpty()) {
                user.setEmail(request.getEmail());
                user.setEmailVerified(Boolean.valueOf(request.getEmail_verified()));
                updated = true;
            }
            if (request.getName() != null && !request.getName().trim().isEmpty()) {
                user.setFirstName(request.getName());
                updated = true;
            }
            if (request.getFamily_name() != null && !request.getFamily_name().trim().isEmpty()) {
                user.setLastName(request.getFamily_name());
                updated = true;
            }
            if (request.getEnabled()!= null && !request.getEnabled().trim().isEmpty()) {
                user.setEnabled(Boolean.valueOf(request.getEnabled()));
                updated = true;
            }
            if(request.getRequiredActions() != null) {
                List<String> requiredActionList = new ArrayList<>();
                requiredActionList.addAll(Arrays.asList(request.getRequiredActions()));
                user.setRequiredActions(requiredActionList);
            }
            if(request.getCredentials() != null){
                // Define password credential
                CredentialRepresentation passwordCred = new CredentialRepresentation();
                
                List<CredentialRepresentation> cred = request.getCredentials();
                passwordCred.setValue(cred.get(0).getValue());
                passwordCred.setType(cred.get(0).getType());
                passwordCred.setTemporary(cred.get(0).isTemporary());

                user.setCredentials(cred);
            }
            if(request.getAttributes() != null) {
                Map<String, List<String>> attr = new HashMap<>();
                
                Iterator it = request.getAttributes().entrySet().iterator();
                while (it.hasNext()) {
                    List<String> list = new ArrayList<>();
                    Map.Entry pair = (Map.Entry)it.next(); 
                    list.add(pair.getValue().toString());
                    attr.put(pair.getKey().toString(), list);
                    it.remove(); // avoids a ConcurrentModificationException
                }

                user.setAttributes(attr);
            }
            
            if (updated) {
                // Update User
                String URL = UPDATE_USER.replace("{realm}", REALM).replace("{id}", user.getId());
                String response = Util.executePUTRequest(MAPPER.writeValueAsString(user), URL, ACCESS_TOKEN);
                if(response.isEmpty())
                {
                    result = true;
                }else {
                    LOGGER.error(String.format("UPDATE USER: ERROR: Unable to update the user\n%s", response));
                }
            }
        } catch(Exception ex) {
            LOGGER.error("ERROR --> "  + ex.getMessage());
        }
        return result;
    }
    
    public static boolean deleteUser(String username) {
        boolean result = false;

        try 
        {
            AdminInitiate();
            
            // Get User
            Map<String, String> user = getUserAttributes(username);
            if(user.size() >= 1)
            {
                // Delete User
                String URL = DELETE_USER.replace("{realm}", REALM).replace("{id}", user.get("id"));
                
                String response = Util.executeDELETERequest(URL, ACCESS_TOKEN);
                if(response.isEmpty())
                {
                    result = true;
                }else {
                    LOGGER.error(String.format("DELETE USER: ERROR: Unable to delete the user\n%s", response));
                }
            } else {
                LOGGER.error(String.format("DELETE USER: ERROR: User not found\n"));
            }
        }
        catch(Exception ex) {
            LOGGER.error("ERROR --> "  + ex.getMessage());
        }
        return result;
    }
    
    private static Map<String, String> getUserAttributes(String username) {
        Map<String, String> props = new LinkedHashMap<>();
        
        AdminInitiate();
        HashMap<String, String> attributes = new HashMap<>();
        attributes.put("username", username);
        List<UserRepresentation> users = getUsers(attributes);
        if(users.size() == 1)
        {
            for(UserRepresentation user : users)
            {
                props.put("id", user.getId());
                props.put("email", user.getEmail());
                props.put("firstName", user.getFirstName());
                props.put("lastName", user.getLastName());
                props.put("username", user.getUsername());
            }
        } else {
            System.out.printf("Found %s users%n", users.size());
        }
        return props;
    }
    
    public static List<UserRepresentation> getUsers(HashMap<String, String> request)  {
        List<UserRepresentation> users = new ArrayList<>();
        try {
            AdminInitiate();
            String URL = USERS.replace("{realm}", REALM);
            UserRepresentation[] response = MAPPER.readValue(Util.executeGETRequest(MAPPER.writeValueAsString(request), URL, ACCESS_TOKEN), UserRepresentation[].class);
            users = Arrays.asList(response);
        } catch (Exception ex) {
            LOGGER.error(String.format("GET USERS: Unable to get users\n%s", ex.getLocalizedMessage()));
        }        
        return users;
    }
    
    public static String getAccessTokenString(String request) {
        AccessTokenResponse tokenResponse = getAccessTokenResponse(request);
        return tokenResponse == null ? null : tokenResponse.getToken();
    }

    private static AccessTokenResponse getAccessTokenResponse(String request) {
        try {
            String URL = GET_TOKEN.replace("{realm}", REALM);
            AccessTokenResponse response = MAPPER.readValue(Util.getToken(URL, request), AccessTokenResponse.class);
            return response;
        } catch (Exception ex) {
            LOGGER.error(String.format("GET ACCESS TOKEN: Unable to get accessToken\n%s", ex.getLocalizedMessage()));
            return null;
        }
    }

    public static List<String> getRoles(String username) {
        List<String> roles = new ArrayList<>();
        try {
            AdminInitiate();
            // Get User
            Map<String, String> user = getUserAttributes(username);
            if(user.size() >= 1)
            {
                // Get User Groups
                String URL = USER_GROUPS.replace("{realm}", REALM).replace("{id}", user.get("id"));

                GroupRepresentation[] response = MAPPER.readValue(Util.executeGETRequest("", URL, ACCESS_TOKEN), GroupRepresentation[].class);
                if (response != null) {
                    for (int i=0; i < response.length; i++) {
                        roles.add(response[i].getName());
                    }
                }
            }
        } catch (Exception ex) {
            LOGGER.error(String.format("GET USER GROUPS: ERROR: Unable to get the user groups\n%s", ex));
        }
        return roles;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.rsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value = "RSA Response", description = "RSA Response")
public class RSAResult<T> {
    private T result;

    public RSAResult(T result) {
        this.result = result;
    }

    @ApiModelProperty(value = "RSA Result", required = true)
    public T getResult() {
        return result;
    }

    public void setResult(T result) {
        this.result = result;
    }
}

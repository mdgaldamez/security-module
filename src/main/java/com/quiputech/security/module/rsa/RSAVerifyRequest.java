/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.rsa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author peter
 */
@ApiModel(value = "RSA Verify Request", description = "RSA Verify Request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RSAVerifyRequest extends RSARequest {
    private String signature;

    @ApiModelProperty(value = "Signature", required = true)
    public String getSignature() {
        return signature;
    }

    public void setSignature(String signature) {
        this.signature = signature;
    }
}

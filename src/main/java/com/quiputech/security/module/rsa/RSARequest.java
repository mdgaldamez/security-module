/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.rsa;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author peter
 */

@ApiModel(value = "RSA Request", description = "RSA Request")
@JsonIgnoreProperties(ignoreUnknown = true)
public class RSARequest {
    private String key;
    private String payload;

    @ApiModelProperty(value = "Key HEX", required = true)
    public String getKey() {
        return key;
    }

    public void setKey(String privateKey) {
        this.key = privateKey;
    }

    @ApiModelProperty(value = "Payload", required = true)
    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    }
}

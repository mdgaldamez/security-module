/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.rsa;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author peter
 */
@ApiModel(value = "RSA Pair Keys", description = "RSA Pair Keys Generated")
public class RSAPairKeys {
    private String publicKey;
    private String privateKey;

    public RSAPairKeys(String publicKey, String privateKey) {
        this.publicKey = publicKey;
        this.privateKey = privateKey;
    }

    @ApiModelProperty(value = "Public Key HEX", required = true)
    public String getPublicKey() {
        return publicKey;
    }

    public void setPublicKey(String publicKey) {
        this.publicKey = publicKey;
    }

    @ApiModelProperty(value = "Private Key Hex", required = true)
    public String getPrivateKey() {
        return privateKey;
    }

    public void setPrivateKey(String privateKey) {
        this.privateKey = privateKey;
    }
}

package com.quiputech.security.module;

import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.dto.SecurityResponse;
import com.quiputech.security.module.entities.CredentialRepresentation;
import com.quiputech.security.module.entities.Settings;
import com.quiputech.security.module.entities.User;
import com.quiputech.security.module.entities.UserRequest;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdámez
 */
public class AuthenticationManager {
    
    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationManager.class);
    
    public static String AuthenticationManager(String appId) {
        String mode = "";
        if(appId != null && !appId.isEmpty()) {
            DatabaseManager db = new DatabaseManager(); 
            Settings settings = db.getSettingsByClientId(appId);
            if(settings.getMode() == 0)
            {
                mode = "cognito";
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                CognitoManager.AWS_ISSUER_URL =  String.format(SecurityConstants.COGNITO_IDENTITY_POOL_URL, CognitoManager.REGION, CognitoManager.USERPOOLID);
                CognitoManager.AWS_JWK_URL = String.format(SecurityConstants.COGNITO_IDENTITY_POOL_URL + SecurityConstants.JSON_WEB_TOKEN_SET_URL_SUFFIX, CognitoManager.REGION, CognitoManager.USERPOOLID);

            }else {
                mode = "redHat";
                RedHatManager.CLIENT_ID = settings.getClientId();
                RedHatManager.CLIENT_SECRET = settings.getClientSecret();
                RedHatManager.USERNAME = settings.getUsername();
                RedHatManager.PASSWORD = settings.getPassword();
                RedHatManager.REALM = settings.getRealm();
            }
        }
        return mode;
    }
    
    public static SecurityResponse authenticate(String[] appId, String username, String password) {
        SecurityResponse response = null;
        String mode = AuthenticationManager(appId[0]);
        DatabaseManager db = new DatabaseManager();
        
        User user = db.getUser(appId[0], username);
        if(user.getUsername() != null) {
            if(mode.equals("cognito")) {
                response = CognitoManager.createToken(username, password);
            }else {
                response = RedHatManager.createToken(username, password);
            }
        } else {
           response = CognitoManager.createToken(username, password);
           if(response != null) {
               mode = "redhat";
               AuthenticationManager(appId[1]);
               String[] requiredActions = {"UPDATE_PASSWORD"};
               
               List<CredentialRepresentation> credentials = new ArrayList<>();
               CredentialRepresentation credential = new CredentialRepresentation();
               credential.setTemporary(true);
               credential.setType("password");
               credential.setValue("temp");
               credentials.add(credential);
                
               UserRequest userRequest = new UserRequest();
               userRequest.setEnabled(response.getAttributes().get("enabled"));
               userRequest.setUsername(response.getAttributes().get("username"));
               userRequest.setEmail(response.getAttributes().get("email"));
               userRequest.setName(!"".equals(response.getAttributes().get("name")) ? response.getAttributes().get("name") : response.getAttributes().get("given_name"));
               //userRequest.setGiven_name(response.getAttributes().get("given_name"));
               userRequest.setFamily_name(response.getAttributes().get("family_name"));
               userRequest.setEmail_verified(response.getAttributes().get("email_verified"));
               userRequest.setRequiredActions(requiredActions);
               userRequest.setCredentials(credentials);
               
               boolean created = RedHatManager.createUser(userRequest);
               if(created)
               {
                   if(db.insertUser(appId[1], mode, userRequest) > 0){
                        LOGGER.info("Usuario registrado en RedHat exitosamente");
                   }
               }
           }
        }
        return response;
    }    
}

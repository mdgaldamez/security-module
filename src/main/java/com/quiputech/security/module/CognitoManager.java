package com.quiputech.security.module;

import com.amazonaws.auth.AWSCredentialsProviderChain;
import com.amazonaws.auth.AWSStaticCredentialsProvider;
import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.auth.InstanceProfileCredentialsProvider;
import com.amazonaws.auth.SystemPropertiesCredentialsProvider;
import com.amazonaws.auth.profile.ProfileCredentialsProvider;
import com.quiputech.security.module.entities.*;
import com.amazonaws.regions.Region;
import com.amazonaws.regions.Regions;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProvider;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClient;
import com.amazonaws.services.cognitoidp.AWSCognitoIdentityProviderClientBuilder;
import com.amazonaws.services.cognitoidp.model.*;
import com.auth0.jwk.Jwk;
import com.auth0.jwk.JwkException;
import com.auth0.jwk.JwkProvider;
import com.auth0.jwk.UrlJwkProvider;
import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import com.auth0.jwt.interfaces.RSAKeyProvider;
import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.dto.SecurityResponse;
import com.quiputech.security.module.utils.StringUtil;
import com.quiputech.security.module.annotations.*;
import com.quiputech.security.module.dto.UserResponse;
import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.net.URL;
import java.security.SecureRandom;
import java.security.interfaces.RSAPrivateKey;
import java.security.interfaces.RSAPublicKey;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.container.ResourceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author mgaldamez
 */
public class CognitoManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(CognitoManager.class);
    protected static String TEMPORARYPASSWORD;
    public static String USERPOOLID;
    public static String CLIENTID;
    public static String REGION;
    public static String AWS_ISSUER_URL;
    public static String AWS_JWK_URL;
    private static JwkProvider JWK_PROVIDER;
    protected static AWSCognitoIdentityProviderClient cognitoClient = new AWSCognitoIdentityProviderClient();

    public static boolean addUserToGroup(String username, String group) {
        boolean result = false;
        if (username == null || username.isEmpty()) {
            return result;
        }

        try {
            setRegion();
            AdminAddUserToGroupRequest cognitoRequest = new AdminAddUserToGroupRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(username)
                    .withGroupName(group);

            cognitoClient.adminAddUserToGroup(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            LOGGER.error(String.format("ADD USER TO GROUP ERROR: Unable to add the user to group\n%s", group, ex.getLocalizedMessage()));
            result = false;
        }

        return result;
    }

    public static boolean createCognitoAccount(UserRequest user) {
        boolean result = false;
        if (user.getEmail().isEmpty() || user.getEmail() == null) {
            return result;
        }

        try {
            setRegion();            
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(user.getUsername())
                    .withUserAttributes(setUserAttributes(user))
                    .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                    .withForceAliasCreation(Boolean.FALSE);

            cognitoClient.adminCreateUser(cognitoRequest);
            if(user.getGroup() != null && !user.getGroup().isEmpty()) {
                addUserToGroup(user.getUsername(), user.getGroup());
            }
            result = true;
        } catch (Exception ex) {
            LOGGER.error(String.format("CREATE USER: ERROR: Unable to create the user\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }
    
    public static boolean createCognitoAccountWithPassword(UserRequest user) {
        boolean result = false;
        if (user.getEmail().isEmpty() || user.getEmail() == null) {
            return result;
        }

        try {
            SecureRandom number = new SecureRandom();
            TEMPORARYPASSWORD = number.toString();
            Map<String,String> initialParams = new HashMap<>();
            initialParams.put("USERNAME", user.getEmail());
            initialParams.put("PASSWORD", TEMPORARYPASSWORD);
            
            setRegion();
            AdminCreateUserRequest cognitoRequest = new AdminCreateUserRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(user.getUsername())
                    .withUserAttributes(setUserAttributes(user))
                    .withTemporaryPassword(TEMPORARYPASSWORD)
                    .withMessageAction("SUPPRESS")
                    .withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
                    .withForceAliasCreation(Boolean.FALSE);
            
            cognitoClient.adminCreateUser(cognitoRequest);

            //.withDesiredDeliveryMediums(DeliveryMediumType.EMAIL)
            /*AdminConfirmSignUpRequest signupRequest = new AdminConfirmSignUpRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminConfirmSignUp(signupRequest);*/
            
             AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                    .withAuthParameters(initialParams)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID);

            AdminInitiateAuthResult initialResponse = cognitoClient.adminInitiateAuth(initialRequest);
            if (!ChallengeNameType.NEW_PASSWORD_REQUIRED.name().equals(initialResponse.getChallengeName()))
            {
                throw new IOException("unexpected challenge: " + initialResponse.getChallengeName());
            }

            Map<String,String> challengeResponses = new HashMap<>();
            challengeResponses.put("USERNAME", user.getUsername());
            challengeResponses.put("PASSWORD", TEMPORARYPASSWORD);
            challengeResponses.put("NEW_PASSWORD", user.getPassword());

            AdminRespondToAuthChallengeRequest finalRequest = new AdminRespondToAuthChallengeRequest()
                    .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    .withChallengeResponses(challengeResponses)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID)
                    .withSession(initialResponse.getSession());

            AdminRespondToAuthChallengeResult challengeResponse = cognitoClient.adminRespondToAuthChallenge(finalRequest);
            
            updateStatusAccount(user.getUsername(), Boolean.valueOf(user.getEnabled()));

            if (challengeResponse != null && challengeResponse.getChallengeName() != null) {
                if(!challengeResponse.getChallengeName().isEmpty()) {
                    throw new IOException("unexpected challenge: " + challengeResponse.getChallengeName());
                }
            }
            
        } catch (Exception ex) {
            result = false;
        }
        return result;
    }
    
    public static void updateStatusAccount(String username, boolean enabled) {
        try {
            if (enabled) {
                AdminEnableUserRequest enableRequest = new AdminEnableUserRequest()
                        .withUserPoolId(USERPOOLID)
                        .withUsername(username);

                cognitoClient.adminEnableUser(enableRequest);
            } else {
                AdminDisableUserRequest disableRequest = new AdminDisableUserRequest()
                        .withUserPoolId(USERPOOLID)
                        .withUsername(username);

                cognitoClient.adminDisableUser(disableRequest);
            }
        }catch(Exception ex) {
            LOGGER.error(String.format("UPDATE STATUS: ERROR: Unable to update the status\n%s", ex.getMessage()));
        }
    }

    public static boolean updateCognitoAccount(UserRequest user) {
        boolean result = false;
        if (user.getUsername().isEmpty() || user.getUsername()== null) {
            return result;
        }

        try {
            setRegion();
            AdminUpdateUserAttributesRequest cognitoRequest = new AdminUpdateUserAttributesRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(user.getUsername())
                    .withUserAttributes(setUserAttributes(user));

            cognitoClient.adminUpdateUserAttributes(cognitoRequest);
            
            if(user.getEnabled() != null){
                updateStatusAccount(user.getUsername(), Boolean.valueOf(user.getEnabled()));
            }
            result = true;
        } catch (Exception ex) {
            LOGGER.error(String.format("UPDATE USER: ERROR: Unable to update the user\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }
    
    public static boolean deleteCognitoAccount(String username) {
        boolean result;
        try {
            setRegion();
            AdminDeleteUserRequest cognitoRequest = new AdminDeleteUserRequest()
                .withUserPoolId(USERPOOLID)
                .withUsername(username);
            cognitoClient.adminDeleteUser(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            LOGGER.error(String.format("DELETE USER: ERROR: Unable to delete the user\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }

    public static GetUserResult getUserByToken(String accessToken) {
        GetUserRequest authRequest = new GetUserRequest().withAccessToken(accessToken);
        return cognitoClient.getUser(authRequest);
    }
        
    public static AdminGetUserResult getUserByUsername(String username) {
        AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                        .withUserPoolId(USERPOOLID)
                        .withUsername(username);
        AdminGetUserResult userResult = cognitoClient.adminGetUser(adminGetUserRequest);
        return userResult;
    }
    
    public static List<String> getListUser() {
        setRegion();
        ListUsersRequest listUsersRequest = new ListUsersRequest()
                .withUserPoolId(USERPOOLID);

        ListUsersResult listUsersResult = cognitoClient.listUsers(listUsersRequest);
        List<String> users = new ArrayList<>();
        if (listUsersResult != null) {
            for (UserType u : listUsersResult.getUsers()) {
                users.add(u.getUsername());
            }
        }
        return users;
    }
        
    public static List<CognitoUser> getListUsers(UserFilterRequest request) {
        List<CognitoUser> users = new ArrayList<>();
        Collection<String> attributesToGet = null;
        if(request.getAttributesToGet() != null && request.getAttributesToGet().length > 0) {
            attributesToGet = new ArrayList<String>();
            for (String attribute: request.getAttributesToGet())
            {
                attributesToGet.add(attribute);
            }
        }
        try {

            setRegion();
            ListUsersRequest listUsersRequest = new ListUsersRequest()
                .withUserPoolId(USERPOOLID)
                .withLimit(10)
                .withAttributesToGet(attributesToGet)
                .withFilter(String.format("%s%s\"%s\"", request.getAttribute_name(), request.getFilter_type(), request.getAttribute_value()));  
            
            ListUsersResult listUsersResult = cognitoClient.listUsers(listUsersRequest);
            if (listUsersResult != null) {                
                for (UserType u : listUsersResult.getUsers()) {
                    HashMap<String, String> attributes = new HashMap<>();
                    HashMap<String, String> mfaoptTypes = new HashMap<>();
                    CognitoUser user = new CognitoUser();
                    user.setEnabled(u.getEnabled());
                    if (u.getMFAOptions() != null) {
                        List<MFAOptionType> mfaOptionTypeList = u.getMFAOptions();
                        for(MFAOptionType mfaoptType: mfaOptionTypeList) {
                            mfaoptTypes.put("AttributeName", mfaoptType.getAttributeName());
                            mfaoptTypes.put("DeliveryMedium", mfaoptType.getDeliveryMedium());
                        }
                        user.setMfaOptions(mfaoptTypes);
                    }
                    user.setUserCreateDate(u.getUserCreateDate());
                    user.setUserLastModifiedDate(u.getUserLastModifiedDate());
                    user.setUserStatus(u.getUserStatus());
                    user.setUsername(u.getUsername());
                    List<AttributeType> attributeList = u.getAttributes();
                    for (AttributeType attribute : attributeList) {
                        attributes.put(attribute.getName(), attribute.getValue());
                    }
                    user.setAttributes(attributes);
                    users.add(user);
                }
            
                String token = listUsersResult.getPaginationToken();
                while (token != null) {
                    listUsersRequest.withPaginationToken(token);
                    listUsersResult = cognitoClient.listUsers(listUsersRequest);
                    if (listUsersResult != null) {
                        for (UserType u : listUsersResult.getUsers()) {
                            HashMap<String, String> attributes = new HashMap<>();
                            HashMap<String, String> mfaoptTypes = new HashMap<>();
                            CognitoUser user = new CognitoUser();
                            user.setEnabled(u.getEnabled());
                            if (u.getMFAOptions() != null) {
                                List<MFAOptionType> mfaOptionTypeList = u.getMFAOptions();
                                for(MFAOptionType mfaoptType: mfaOptionTypeList) {
                                    mfaoptTypes.put("AttributeName", mfaoptType.getAttributeName());
                                    mfaoptTypes.put("DeliveryMedium", mfaoptType.getDeliveryMedium());
                                }
                                user.setMfaOptions(mfaoptTypes);
                            }
                            user.setUserCreateDate(u.getUserCreateDate());
                            user.setUserLastModifiedDate(u.getUserLastModifiedDate());
                            user.setUserStatus(u.getUserStatus());
                            user.setUsername(u.getUsername());
                            List<AttributeType> attributeList = u.getAttributes();
                            for (AttributeType attribute : attributeList) {
                                attributes.put(attribute.getName(), attribute.getValue());
                            }
                            user.setAttributes(attributes);
                            users.add(user);
                        }
                        token = listUsersResult.getPaginationToken();
                    }                    
                }
            }
        } catch (InvalidParameterException ex) {
            LOGGER.error(String.format("LIST USERS: ERROR: Unable to get the users\n%s", ex.getMessage()));
            return null;
        }
        return users;
    }

    public static boolean resetCognitoPassword(String email) {
        boolean result = false;
        if (email == null || email.isEmpty()) {
            return result;
        }

        try {
            setRegion();
            AdminResetUserPasswordRequest cognitoRequest = new AdminResetUserPasswordRequest()
                    .withUserPoolId(USERPOOLID)
                    .withUsername(email);

            cognitoClient.adminResetUserPassword(cognitoRequest);
            result = true;
        } catch (Exception ex) {
            LOGGER.error(String.format("RESET PASSWORD: ERROR: Unable to reset the password\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }

    public static boolean confirmForgotPassword(UserRequest user) {
        boolean result = false;
        if (user.getUsername()== null || user.getUsername().isEmpty()) {
            return result;
        }
        try {
            setRegion();
            ConfirmForgotPasswordRequest cognitoRequest = new ConfirmForgotPasswordRequest()
                        .withClientId(CLIENTID)
                        .withConfirmationCode(user.getConfirmation_code())
                        .withPassword(user.getPassword())
                        .withUsername(user.getUsername());
            cognitoClient.confirmForgotPassword(cognitoRequest);
            result = true;
        }catch(Exception ex)
        {
            LOGGER.error(String.format("CONFIRM PASSWORD: ERROR: Unable to confirm the password\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }
    
    public static boolean changePassword(UserRequest user) {
        boolean result = false;
        if (user.getUsername() == null || user.getUsername().isEmpty()) {
            return result;
        }
        try {
            // must attempt signin with temporary password in order to establish session for password change
            // (even though it's documented as not required)

            Map<String,String> initialParams = new HashMap<>();
            initialParams.put("USERNAME", user.getUsername());
            initialParams.put("PASSWORD", user.getPassword());
            
            setRegion();
            AdminInitiateAuthRequest initialRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                    .withAuthParameters(initialParams)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID);

            AdminInitiateAuthResult initialResponse = cognitoClient.adminInitiateAuth(initialRequest);
            if (! ChallengeNameType.NEW_PASSWORD_REQUIRED.name().equals(initialResponse.getChallengeName()))
            {
                throw new RuntimeException("unexpected challenge: " + initialResponse.getChallengeName());
            }

            Map<String,String> challengeResponses = new HashMap<>();
            challengeResponses.put("USERNAME", user.getUsername());
            challengeResponses.put("PASSWORD", user.getPassword());
            challengeResponses.put("NEW_PASSWORD", user.getNew_password());

            AdminRespondToAuthChallengeRequest finalRequest = new AdminRespondToAuthChallengeRequest()
                    .withChallengeName(ChallengeNameType.NEW_PASSWORD_REQUIRED)
                    .withChallengeResponses(challengeResponses)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID)
                    .withSession(initialResponse.getSession());

            AdminRespondToAuthChallengeResult challengeResponse = cognitoClient.adminRespondToAuthChallenge(finalRequest);
            if (challengeResponse != null && challengeResponse.getChallengeName() != null) {
                if(!challengeResponse.getChallengeName().isEmpty()) {
                    throw new RuntimeException("unexpected challenge: " + challengeResponse.getChallengeName());
                }
            }
            result = true;
        }catch(Exception ex)
        {
            LOGGER.error(String.format("CHANGE PASSWORD: ERROR: Unable to change the password\n%s", ex.getMessage()));
            result = false;
        }
        return result;
    }
    
    public static AdminInitiateAuthResult refreshToken(String refreshToken) {
        Map<String, String> authParams = new HashMap<>();
        authParams.put("REFRESH_TOKEN", refreshToken);

        AdminInitiateAuthRequest refreshRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.REFRESH_TOKEN)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        return cognitoClient.adminInitiateAuth(refreshRequest);
    }
    
    public static SecurityResponse attemptRefresh(String refreshToken) throws IOException {

        try {
            Map authParams = new HashMap();
            authParams.put("REFRESH_TOKEN", refreshToken);

            setRegion();
            AdminInitiateAuthRequest refreshRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.REFRESH_TOKEN)
                    .withAuthParameters(authParams)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID);

            AdminInitiateAuthResult refreshResponse = cognitoClient.adminInitiateAuth(refreshRequest);
            if (StringUtil.isNullOrEmpty(refreshResponse.getChallengeName())) {
                LOGGER.debug("successfully refreshed token");
                return SecurityResponse.builder(refreshResponse.getAuthenticationResult()).build();
            } else {
                LOGGER.warn("unexpected challenge when refreshing token: {}", refreshResponse.getChallengeName());
            }
        } catch (TooManyRequestsException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } catch (AWSCognitoIdentityProviderException ex) {
            LOGGER.debug("exception during token refresh: {}", ex.getMessage());
        }
        return null;
    }
        
    public static CognitoAccount authenticateAccount(String userName, String password) {
        CognitoAccount account = null;
        try {
            CognitoAuthenticationInfo auth = authenticate(userName, password);
            
            if (auth != null) {
                List<String> roles = getRoles(userName);
                account = new CognitoAccount();
                account.setEmail(userName);
                account.setGroups(roles);
                account.setRefreshToken(userName);
                account.setAccessToken(auth.getAccessToken());
                account.setAttributes(getUserAttributes(userName, auth.getAccessToken()));
            }

        } catch (Exception ex) {
            LOGGER.error(String.format("AUTH USER: ERROR: Unable to authenticate the user\n%s", ex.getMessage()));
        }
        return account;
    }
        
    private static CognitoAuthenticationInfo authenticate(String username, String password) {

        Map<String, String> authParams = new HashMap<>();
        authParams.put("USERNAME", username);
        authParams.put("PASSWORD", password);

        setRegion();
        AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
                .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                .withAuthParameters(authParams)
                .withClientId(CLIENTID)
                .withUserPoolId(USERPOOLID);

        AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);

        if (authResponse.getChallengeName() == null) {
            PrincipalCollection principals = new PrincipalCollection();
            String authtoken = "";
            try {
                authtoken = authResponse.getAuthenticationResult().getAccessToken();
                principals.setPrincipal(CognitoManager.class.getName());
                principals.setPrincipalCollection(createPrincipals(username, authtoken));
            } catch (Exception e) {
                LOGGER.error("Unable to obtain authenticated account properties.", e);
            }            
            return new CognitoAuthenticationInfo(principals, authtoken);
        }
        return null;
    }
    
    public static SecurityResponse createToken(String userName, String password) {
        try {
            Map<String, String> authParams = new HashMap<>();
            authParams.put("USERNAME", userName);
            authParams.put("PASSWORD", password);
            
            setRegion();
            AdminInitiateAuthRequest authRequest = new AdminInitiateAuthRequest()
                    .withAuthFlow(AuthFlowType.ADMIN_NO_SRP_AUTH)
                    .withAuthParameters(authParams)
                    .withClientId(CLIENTID)
                    .withUserPoolId(USERPOOLID);
            
            AdminInitiateAuthResult authResponse = cognitoClient.adminInitiateAuth(authRequest);
            List<String> roles = getRoles(userName);
            SecurityResponse response = SecurityResponse.builder(authResponse.getAuthenticationResult()).build();
            response.setGroups(roles);
            response.setAttributes(getUserAttributes(userName, response.getAccessToken()));
            return response;
        } catch (Exception e) {
             LOGGER.error("Unable to obtain access token.\n", e);
            return null;
        }
    }
    
    public static boolean hasSecurity(ResourceInfo resourceInfo) {
        final Class<?> declaring = resourceInfo.getResourceClass();
        final Method method = resourceInfo.getResourceMethod();

        if (declaring == null || method == null) {
            return true;
        }

        return !method.isAnnotationPresent(NoSecurity.class);
    }

    public static UserResponse validateToken(String token) {
        UserResponse response = UserResponse.builder().build();

        try {
            if (token != null && !token.isEmpty() && !token.contains("Basic ")) {
                DecodedJWT jwt = JWT.decode(token);

                if (jwt.getKeyId() != null && !jwt.getKeyId().isEmpty()) {

                    RSAKeyProvider keyProvider = retrieveKeyProvider();

                    if (keyProvider.getPublicKeyById(jwt.getKeyId()) != null) {
                        Algorithm algorithm = Algorithm.RSA256(keyProvider);

                        try {
                            JWTVerifier verifier = JWT.require(algorithm)
                                    .withIssuer(AWS_ISSUER_URL)
                                    .build(); //Reusable verifier instance
                            jwt = verifier.verify(token);

                            if (jwt != null) {
                                String userPoolId = getCognitoPoolId(jwt.getIssuer());

                                if (userPoolId != null && userPoolId.equalsIgnoreCase(USERPOOLID)) {
                                    setRegion();
                                    GetUserRequest authRequest = new GetUserRequest().withAccessToken(token);
                                    GetUserResult authResponse = cognitoClient.getUser(authRequest);
                                    response = UserResponse.builder(true, authResponse.getUsername(), userPoolId).build();
                                }
                            }
                        } catch (JWTVerificationException exception) {
                            //Invalid signature/claims
                            LOGGER.error(exception.getLocalizedMessage(), exception);
                        }
                    }
                }
            }
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
        }
        return response;
    }
    
    private static RSAKeyProvider retrieveKeyProvider() {

        RSAKeyProvider keyProvider = new RSAKeyProvider() {
            @Override
            public RSAPublicKey getPublicKeyById(String kid) {
                try {
                    //Received 'kid' value might be null if it wasn't defined in the Token's header
                    Jwk jwk = retrieveProvider().get(kid); //throws Exception when not found or can't get one
                    return (RSAPublicKey) jwk.getPublicKey();
                } catch (JwkException ex) {
                    LOGGER.error(ex.getLocalizedMessage(), ex);
                }
                return null;
            }

            @Override
            public RSAPrivateKey getPrivateKey() {
                return null;
            }

            @Override
            public String getPrivateKeyId() {
                return null;
            }
        };

        return keyProvider;
    }
    
    private static JwkProvider retrieveProvider() {
        try {
            if (JWK_PROVIDER == null) {
                JWK_PROVIDER = new UrlJwkProvider(new URL(AWS_JWK_URL));
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return JWK_PROVIDER;
    }
    
    private static String getCognitoPoolId(String url) {
        String result = null;
        if (url != null) {
            String[] tmp = url.split("\\/");

            if (tmp.length >= 4) {
                result = tmp[3];
            }
        }

        return result;
    }
    
    protected static HashMap<String, Object> createPrincipals(String username, String accessToken) {
        HashMap<String, Object> principals = new HashMap<>();
        principals.put("username", username);
        principals.put("attributes", getUserAttributes(username, accessToken));
       
        return principals;
    }
        
    public static List<String> getRoles(String username) {
        List<String> roles = new ArrayList<>();
        AdminListGroupsForUserRequest groupRequest = new AdminListGroupsForUserRequest()
                .withUsername(username)
                .withUserPoolId(USERPOOLID);
        try {
            AdminListGroupsForUserResult groupResult = cognitoClient.adminListGroupsForUser(groupRequest);
            
            if (groupResult != null) {
                for (GroupType g : groupResult.getGroups()) {
                    roles.add(g.getGroupName());
                }
            }
        } catch (Exception e) {
            
        }
        return roles;
    }
    
    private static Map<String, String> getUserAttributes(String username, String accesstoken) {
        Map<String, String> props = new LinkedHashMap<>();

        nullSafePut(props, "username", username);
        nullSafePut(props, "email", username);

        GetUserRequest userRequest = new GetUserRequest().withAccessToken(accesstoken);
        GetUserResult userResponse = cognitoClient.getUser(userRequest);

        for (AttributeType att : userResponse.getUserAttributes()) {
            nullSafePut(props, att.getName(), att.getValue());
        }

        return props;
    }
    
    public static Map<String, String> getUserAttributes(String username){
        try {
            Map<String, String> props = new LinkedHashMap<>();
            
            setRegion();
            AdminGetUserRequest adminGetUserRequest = new AdminGetUserRequest()
                .withUserPoolId(USERPOOLID)
                .withUsername(username);
            AdminGetUserResult adminGetUserResult = cognitoClient.adminGetUser(adminGetUserRequest);
            //FORCE_CHANGE_PASSWORD
            if(adminGetUserResult.getUserStatus().equals("CONFIRMED")){
                List<AttributeType> attributes = adminGetUserResult.getUserAttributes();

                if (attributes != null) {
                    for (AttributeType u : attributes) {
                        nullSafePut(props, u.getName(), u.getValue());
                    }
                }
            }
            return props;
        } catch (Exception ex) {
            LOGGER.error("Error al intentar obtener los atributos");
            return null;
        }
    }

    private static void nullSafePut(Map<String, String> props, String propName, String value) {
        String cleanValue = value.trim();
        if (cleanValue != null) {
            props.put(propName, cleanValue);
        }
    }
    
    public static void setRegion() {
        switch(REGION.toLowerCase()){
            case "us-east-2":
                cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_2));
                break;
            case "us-east-1":
                cognitoClient.setRegion(Region.getRegion(Regions.US_EAST_1));
                break;
        }
    }
    
    public static Collection<AttributeType> setUserAttributes(UserRequest user) {
        Collection<AttributeType> userAttributes = new ArrayList<>();
        HashMap<String, String> attributes = new HashMap<>();
         try {
            Class _class = Class.forName("com.quiputech.security.module.entities.UserRequest");
            Field properties[] = _class.getFields();
            for (int i = 0; i < properties.length; i++) {
                Field field = properties[i];
                // get value of the fields 
                Object value = field.get(user);
                if (value != null && field.getName() != "username" && field.getName() != "password" && field.getName() != "group" && field.getName() != "enabled") {
                    if(field.getName().equals("attributes")) {
                        attributes = (HashMap<String, String>) value;
                        Iterator it = attributes.entrySet().iterator();
                        while (it.hasNext()) {
                            Map.Entry pair = (Map.Entry)it.next();
                            userAttributes.add(new AttributeType().withName("custom:"+pair.getKey().toString()).withValue(pair.getValue().toString())); 
                        }                        
                    } else{
                        userAttributes.add(new AttributeType().withName(field.getName()).withValue(value.toString()));  
                    }
                }
                LOGGER.debug("Name: " + field.getName() + " Value: " + value + " Type: " + field.getType());
            }
        } catch (ClassNotFoundException | IllegalArgumentException | IllegalAccessException ex) {
            LOGGER.error(String.format("SET USER ATTRIBUTES ERROR: Unable to set user attributes\n", ex.getLocalizedMessage()));
        }
        return userAttributes;  
    }
    
    private static AWSCognitoIdentityProvider getCognitoProvider() {
        return AWSCognitoIdentityProviderClientBuilder
                .standard()
                .withCredentials(getAWSCredentials())
                .withRegion(Regions.US_EAST_1)
                .build();
    }

    private static AWSCredentialsProviderChain getAWSCredentials() {

        BasicAWSCredentials basic = new BasicAWSCredentials(SecurityConstants.AWS_ACCESS_KEY_ID, SecurityConstants.AWS_SECRET_ACCESS_KEY);
        return new AWSCredentialsProviderChain(
                new InstanceProfileCredentialsProvider(false),
                new SystemPropertiesCredentialsProvider(),
                new ProfileCredentialsProvider("development"),
                new AWSStaticCredentialsProvider(basic)
        );
    }
}
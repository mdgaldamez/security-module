/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.TextNode;
import com.jayway.jsonpath.Configuration;
import com.jayway.jsonpath.JsonPath;
import com.jayway.jsonpath.Option;
import com.jayway.jsonpath.PathNotFoundException;
import com.jayway.jsonpath.spi.json.JacksonJsonNodeJsonProvider;
import com.jayway.jsonpath.spi.mapper.JacksonMappingProvider;
import com.quiputech.security.module.crypto.CryptoUtil;
import com.quiputech.security.module.dto.SecurityKey;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Peter Galdamez
 */
public class TwoFactorDecrypt {
    private static final Logger LOGGER = LoggerFactory.getLogger(TwoFactorDecrypt.class);
    private static final String SCOPE = "$.%s";
    private static final Configuration CONFIGURATION = Configuration.builder()
            .options(Option.DEFAULT_PATH_LEAF_TO_NULL)
            .jsonProvider(new JacksonJsonNodeJsonProvider())
            .mappingProvider(new JacksonMappingProvider())
            .build();
    
    public static String decrypt(String original, String scope, SecurityKey key) throws PathNotFoundException {
        String result = null;
        try {
            String path = String.format(SCOPE, scope);
            Object value = JsonPath.using(CONFIGURATION).parse(original).read(path);
            
            if (value != null && value instanceof TextNode) {
                String encrypted = ((TextNode) value).textValue();
                String decrypted = CryptoUtil.decrypt(encrypted, key.getBaseKey(), key.getIv(), null);
                JsonNode json = JsonPath.using(CONFIGURATION).parse(original).set(path, decrypted).json();
                if (json != null) {
                    result = json.toString();
                }
            }
        } catch (PathNotFoundException ex) {
            throw ex;
        } catch(Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return result;
    }
}

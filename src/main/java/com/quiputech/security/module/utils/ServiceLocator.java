/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;


import java.util.Map;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.util.concurrent.ConcurrentHashMap;
import com.quiputech.security.module.constants.SecurityConstants;

/**
 *
 * @author Peter
 */
public class ServiceLocator {
    private static final Logger LOGGER = LoggerFactory.getLogger(ServiceLocator.class);

    private Map cacheMap;
    private static ServiceLocator instance;

    private ServiceLocator() {
        try {
            cacheMap = new ConcurrentHashMap<>();
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    public static ServiceLocator getInstance() {
        if (instance == null) {
            instance = new ServiceLocator();
        }
        return instance;
    }

    public DataSource getDataSource(final String datasourceName) {
        DataSource dataSource = null;
        Context context = null;
        try {
            if (cacheMap.containsKey(datasourceName)) {
                dataSource = (DataSource) cacheMap.get(datasourceName);
            } else {
                try {
                    context = new InitialContext();
                    dataSource = (DataSource) context.lookup(datasourceName);
                    cacheMap.put(datasourceName, dataSource);
                } catch(NamingException ex) {
                    LOGGER.error(ex.getLocalizedMessage());
                } finally {
                    if (context != null) {
                        context.close();
                    }
                }
            }
        } catch (NamingException ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } 
        
        return dataSource;
    }
    
    public DataSource getDataSource() {
        return getDataSource(SecurityConstants.ECORE_DATASOURCE);
    }
}

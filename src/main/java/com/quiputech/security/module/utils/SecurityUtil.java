/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import com.jayway.jsonpath.PathNotFoundException;
import com.quiputech.security.module.dto.AuthInformation;
import com.quiputech.security.module.enums.AuthorizeType;
import com.quiputech.security.module.session.SecuritySession;
import java.lang.reflect.Method;
import java.util.List;
import java.util.Map;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ResourceInfo;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.quiputech.security.module.annotations.NoSecurity;
import com.quiputech.security.module.crypto.CryptoUtil;
import com.quiputech.security.module.dto.EncryptedRequest;
import com.quiputech.security.module.dto.Scope;
import com.quiputech.security.module.dto.ScopeRequest;
import com.quiputech.security.module.dto.SecurityKey;
import java.nio.charset.Charset;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import javax.ws.rs.container.ContainerRequestContext;

/**
 *
 * @author Peter
 */
public class SecurityUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(SecurityUtil.class);

    public static boolean hasSecurity(ResourceInfo resourceInfo) {
        final Class<?> declaring = resourceInfo.getResourceClass();
        final Method method = resourceInfo.getResourceMethod();

        if (declaring == null || method == null) {
            return true;
        }

        return !method.isAnnotationPresent(NoSecurity.class);
    }

    public static String sha256(String input) throws NoSuchAlgorithmException {
        MessageDigest md = MessageDigest.getInstance("SHA-256");
        byte[] aMessageDigest = md.digest(input.getBytes(Charset.forName("UTF-8")));
        StringBuilder hexStrBuilder = new StringBuilder();
        for (int i = 0; i < aMessageDigest.length; i++) {
            hexStrBuilder.append(String.format("%02x", aMessageDigest[i]));
        }
        return hexStrBuilder.toString();
    }

    public static String getClientIp(HttpServletRequest request) {
        String remoteAddr = "";

        if (request != null) {
            remoteAddr = request.getHeader("X-FORWARDED-FOR");
            if (remoteAddr == null || "".equals(remoteAddr)) {
                remoteAddr = request.getRemoteAddr();
            }
        }
        return remoteAddr;
    }
    
    /**
     * Exists in list.
     *
     * @param list the list
     * @param value the value
     * @return true, if successful
     */
    public static boolean existsInList(final String list, final String value) {
        return existsInList(list, value, ",");
    }

    /**
     * Exists in list.
     *
     * @param list the list
     * @param value the value
     * @param sep the sep
     * @return true, if successful
     */
    public static boolean existsInList(final String list, final String value, final String sep) {
        boolean result = false;
        if (list != null) {
            String value_s = value != null ? value.trim() : "";
            String sep_s = sep != null ? sep : ",";
            String[] listArray = list.split("\\" + sep_s);
            for (String s : listArray) {
                if (s.trim().equalsIgnoreCase(value_s)) {
                    result = true;
                    break;
                }
            }
        }
        return result;
    }

    public static boolean isUserInRole(String userName, String path, String remoteIp) {
        return isUserInRole(userName, path, null, remoteIp);
    }

    public static boolean isUserInRole(String userName, String path, String identifier, String remoteIp) {
        SecuritySession session = null;
        boolean isIpAllowed = false;
        boolean isIdentifierAllowed = false;
        boolean isMerchantValid = false;
        try {
            session = new SecuritySession();

            String[] paths = null;
            String full = "";

            if (path != null) {
                String local = path.toLowerCase().contains("https://") ? path.substring(8) : path.substring(7);
                paths = local.split("/");

                for (int i = 1; i < paths.length; i++) {
                    full += paths[i] + "/";
                }

                if (!full.isEmpty()) {
                    full = full.substring(0, full.length() - 1);
                }
            }

            List<AuthInformation> list = session.retrieveAuthInformation(userName, full);

            if (list == null || list.isEmpty()) {
                list = session.retrieveAuthInformation("*", full);
            }

            if (list != null && list.size() > 0) {
                for (AuthInformation row : list) {
                    if (row.isValidateMerchant()) {
                        if (paths != null && paths.length > row.getMerchantPosition()) {
                            isMerchantValid = session.isMerchantValid(userName, paths[row.getMerchantPosition()]);
                        }
                    } else {
                        isMerchantValid = true;
                    }

                    if (row.getAuthorizeType().equals(AuthorizeType.ALL)) {
                        isIpAllowed = true;
                        isIdentifierAllowed = true;
                        break;
                    }

                    if (row.getUserName() != null && row.getUserName().equalsIgnoreCase(userName)) {
                        isIpAllowed = row.getRemoteIps() != null && !row.getRemoteIps().equalsIgnoreCase("*") && !row.getRemoteIps().isEmpty() ? existsInList(row.getRemoteIps(), remoteIp) : true;

                        if (row.getIdentifierList() != null && !row.getIdentifierList().isEmpty()) {
                            if (identifier != null && !identifier.isEmpty()) {
                                isIdentifierAllowed = existsInList(row.getIdentifierList(), identifier);
                            } else if (row.isPathIdentifier()) {
                                if (paths != null && paths.length > row.getIdentifierPathPosition() && row.getIdentifierPathPosition() >= 0) {
                                    isIdentifierAllowed = existsInList(row.getIdentifierList(), paths[row.getIdentifierPathPosition()]);
                                }
                            } else if (!row.isPathIdentifier() && identifier == null) {
                                isIdentifierAllowed = true;
                            }
                        } else {
                            isIdentifierAllowed = true;
                        }

                        if (isIpAllowed && isIdentifierAllowed) {
                            break;
                        }
                    }
                }
            } else {
                isIpAllowed = false;
                isIdentifierAllowed = false;
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        } finally {

        }

        return isIpAllowed && isIdentifierAllowed && isMerchantValid;
    }

    public static boolean isIdempotent(String accessToken, String endpoint, String payload) {
        SecuritySession session = null;
        boolean result = false;
        try {
            session = new SecuritySession();
            result = session.isIdempotent(accessToken, endpoint, payload);
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return result;
    }
    
    public static boolean addApiLog(String correlationId, int httpStatus, String path, long startDate, String user, String clientIp, String userAgent, String payload, String response) {
        SecuritySession session = null;
        boolean result = false;
        try {
            session = new SecuritySession();
            result = session.addApiLog(correlationId, httpStatus, path, startDate, user, clientIp, userAgent, payload, response);
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return result;
    }

    public static boolean createKeys(String accessToken, String user, SecurityKey exposed, SecurityKey wrapped) {
        SecuritySession session = null;
        boolean result = false;
        try {
            session = new SecuritySession();
            String tokenId = SecurityUtil.sha256(accessToken);
            result = session.createKeys(tokenId, user, exposed, wrapped);
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return result;
    }

    public static String encrypt(String accessToken, String json) throws PathNotFoundException {
        return processEncryption(accessToken, null, json, false);
    }

    public static String decrypt(String accessToken, String scopeToken, String json) throws PathNotFoundException {
        return processEncryption(accessToken, scopeToken, json, true);
    }

    private static String processEncryption(String accessToken, String scopeToken, String json, boolean decrypt) throws PathNotFoundException {
        SecuritySession session = null;
        String result = json;
        Map<String, SecurityKey> keys = null;

        try {
            EncryptedRequest request = ParseUtil.fromJson(json, EncryptedRequest.class);
            ScopeRequest scopes = ParseUtil.fromJson(scopeToken, ScopeRequest.class);

            boolean isPayloadEncrypted = request != null && request.getPayload() != null;
            boolean isScopePresent = scopes != null && scopes.getScopes() != null;

            if (isPayloadEncrypted || isScopePresent) {
                String tokenId = SecurityUtil.sha256(accessToken);
                session = new SecuritySession();
                keys = session.retrieveKeys(tokenId);
                
                if (keys == null) {
                    throw new RuntimeException(String.format("Token without keys [%s]", tokenId));
                }

                if (isPayloadEncrypted && request != null) {
                    if (request.getKey() != null && !request.getKey().isEmpty()) {
                        SecurityKey currentKey = keys.get(request.getKey());

                        if (currentKey != null) {
                            result = decrypt ? CryptoUtil.decrypt(request.getPayload(), currentKey.getBaseKey(), currentKey.getIv(), null)
                                    : CryptoUtil.encrypt(request.getPayload(), currentKey.getBaseKey(), currentKey.getIv(), null);

                            if (result == null) {
                                throw new RuntimeException(String.format("Encryption [key - %s] is not valid", request.getKey()));
                            }
                        } else {
                            throw new RuntimeException(String.format("Key not found", request.getKey()));
                        }
                    } else {
                        throw new RuntimeException(String.format("Encryption key is not valid", request.getKey()));
                    }
                }

                if (isScopePresent && scopes != null) {
                    for (Scope scope : scopes.getScopes()) {
                        if (scope != null && scope.getKey() != null && !scope.getKey().isEmpty()
                                && scope.getElements() != null) {

                            SecurityKey scopeKey = keys.get(scope.getKey());

                            for (String item : scope.getElements()) {
                                result = TwoFactorDecrypt.decrypt(result, item, scopeKey);

                                if (result == null) {
                                    throw new PathNotFoundException(String.format("Scope [%s] is not valid", item));
                                }
                            }
                        }
                    }
                }
            }
        } catch (PathNotFoundException ex) {
            throw ex;
        } catch (RuntimeException ex) {
            throw ex;
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return result;
    }
    
    /**
     * Determines if the request having the supplied request context has a body
     * or not.
     *
     * @param inRequestContext Request context for request.
     * @return True if request has a body, false otherwise.
     */
    public static boolean isRequestBodyless(ContainerRequestContext inRequestContext) {
        final String theRequestHttpMethod = inRequestContext.getMethod();
        final boolean theBodylessFlag = ("GET".equals(theRequestHttpMethod))
                || ("DELETE".equals(theRequestHttpMethod))
                || ("HEAD".equals(theRequestHttpMethod));

        return theBodylessFlag;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michael Galdámez
 */
public class testSerializer {
    
    
public abstract class MapAsArraySerializerTEST<K,V> extends JsonSerializer<Map<K,V>> {

    @Override
    public void serialize(Map<K, V> map, JsonGenerator generator, SerializerProvider serializerProvider) throws IOException {
        generator.writeStartArray();
        int length = map != null ? map.size() : -1;
        generator.writeNumber(length);
        if(map!= null) {
            for (Map.Entry<K, V> entry : map.entrySet()) {
                generator.writeObject(entry.getKey());
                generator.writeObject(entry.getValue());
            }
        }
        generator.writeEndArray();
    }

    protected abstract Class<V> getValueClass();

    protected abstract Class<K> getKeyClass();
}

public abstract class MapAsArrayDeserializerTEST<K, V> extends JsonDeserializer<Map<K, V>> {

    @Override
    public Map<K, V> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext)
        throws IOException, JsonProcessingException {
        Map<K, V> map = null;

        // Skip START_ARRAY and set the pointer in the first element of the array.
        jsonParser.nextValue();
        int length = jsonParser.getIntValue();

        if(length >= 0) {
            map = new HashMap<K, V>();
            // Set the pointer in the second element of the array
            jsonParser.nextValue();
            for (int i = 0; i < length; i++) {
                K key = jsonParser.readValueAs(getKeyClass());
                V value = jsonParser.readValueAs(getValueClass());
                map.put(key, value);
            }
        }
        // We have to skip the END_ARRAY which wraps the list of elements, in order to return a parser with a pointer in a consistent position
        jsonParser.nextValue();
        return map;
    }

    protected abstract Class<V> getValueClass();

    protected abstract Class<K> getKeyClass();
}
    
}

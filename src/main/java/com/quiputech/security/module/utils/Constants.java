package com.quiputech.security.module.utils;

/**
 *
 * @author mgaldamez
 */
public class Constants {
    private static final boolean DEBUG = Boolean.parseBoolean(System.getProperty("debug", "false"));
    public static final String BASE_URL = DEBUG
        ? "https://access.intvnt.com/auth"
        : "https://access.intvnt.com/auth";
    public static String URL_ADMIN = BASE_URL +"/admin/realms/{realm}";
    
    public static final String URL_OPENID_CONNECT = BASE_URL + "/realms/{realm}/protocol/openid-connect";
    
    public static final String GET_TOKEN = URL_OPENID_CONNECT + "/token";
    public static final String AUTH = URL_OPENID_CONNECT + "/auth";
    public static final String LOGOUT = URL_OPENID_CONNECT + "/logout";
    public static final String USER_INFO = URL_OPENID_CONNECT + "/userinfo";
    
    public static final String USERS = URL_ADMIN + "/users";
    public static final String DELETE_USER = USERS + "/{id}";
    public static final String UPDATE_USER = USERS + "/{id}";
    public static final String USER_GROUPS = USERS + "/{id}/groups";
    
    public static final String DATETIME_FORMAT = "yyMMddHHmmss";
    public static final String DATE_FORMAT = "yyyyMMdd";
    public static final String TIME_FORMAT = "HHmmss";
    public static final String CLIENT_ID = "admin-cli";
    public static final String GRANT_TYPE = "password";
    public static final String METHOD_POST = "POST";
    public static final String METHOD_GET = "GET";
    public static final String PASSWORD = "password";
    
    public static String TOKEN = "{\n" +
                            "	\"grant_type\": \"password\",\n" +
                            "	\"client_id\": \"admin-cli\",\n" +
                            "	\"username\": \"{username}\",\n" +
                            "	\"password\": \"{password}\"\n" +
                            "}";
}

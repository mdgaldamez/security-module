/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author peter
 */
public class MaskedCardUtil {

    public static final Map<String, String> CARDREGEXP = new HashMap<String, String>() {
        {
            put("VISA1", "([^0-9])4[0-9]{8}([0-9]{4})([^0-9]|$)");
            put("VISA2", "([^0-9])4[0-9]{11}([0-9]{4})([^0-9]|$)");
            put("VISA3", "([^0-9])4[0-9]{12}([0-9]{4})([^0-9]|$)");
            put("VISA4", "^4[0-9]{12}(?:[0-9]{3})?$");
            put("MASTERCARD1", "([^0-9])5[12345][0-9]{10}([0-9]{4})([^0-9]|$)");
            put("MASTERCARD2", "([^0-9])5[12345][0-9]{9}([0-9]{4})([^0-9]|$)");
            put("MASTERCARD3", "([^0-9])5[12345][0-9]{11}([0-9]{4})([^0-9]|$)");
            put("MASTERCARD4", "^5[1-5][0-9]{14}$");
            put("AMEX1", "([^0-9])3[47][0-9]{9}([0-9]{4})([^0-9]|$)");
            put("AMEX2", "([^0-9])30[12345][0-9]{8}([0-9]{4})([^0-9]|$)");
            put("AMEX3", "([^0-9])3[47][0-9]{10}([0-9]{4})([^0-9]|$)");
            put("AMEX4", "^3[47][0-9]{13}$");
            put("BGCGLOBAL", "^(6541|6556)[0-9]{12}$");
            put("CARTEBLANCHE", "^389[0-9]{11}$");
            put("DINERS", "([^0-9])3[47][0-9]{8}([0-9]{4})([^0-9]|$)");
            put("DINERS2", "^3(?:0[0-5]|[68][0-9])[0-9]{11}$");
            put("DISCOVER1", "([^0-9])6011[0-9]{8}([0-9]{4})([^0-9]|$)");
            put("DISCOVER2", "([^0-9])6011[0-9]{9}([0-9]{4})([^0-9]|$)");
            put("DISCOVER3", "([^0-9])6011[0-9]{7}([0-9]{4})([^0-9]|$)");
            put("DISCOVER4", "([^0-9])65[0-9]{10}([0-9]{4})([^0-9]|$)");
            put("DISCOVER5", "^65[4-9][0-9]{13}|64[4-9][0-9]{13}|6011[0-9]{12}|(622(?:12[6-9]|1[3-9][0-9]|[2-8][0-9][0-9]|9[01][0-9]|92[0-5])[0-9]{10})$");
            put("INSTA", "^63[7-9][0-9]{13}$");
            put("JCB1", "([^0-9])2131[0-9]{6,8}([0-9]{4})([^0-9]|$)");
            put("JCB2", "([^0-9])1800[0-9]{6,8}([0-9]{4})([^0-9]|$)");
            put("JCB3", "([^0-9])35[0-9]{8,10}([0-9]{4})([^0-9]|$)");
            put("JCB4", "^(?:2131|1800|35\\d{3})\\d{11}$");
            put("KOREAN", "^9[0-9]{15}$");
            put("LASER", "^(6304|6706|6709|6771)[0-9]{12,15}$");
            put("MAESTRO", "^(5018|5020|5038|6304|6759|6761|6763)[0-9]{8,15}$");
            put("SOLO", "^(6334|6767)[0-9]{12}|(6334|6767)[0-9]{14}|(6334|6767)[0-9]{15}$");
            put("SWITCH", "^(4903|4905|4911|4936|6333|6759)[0-9]{12}|(4903|4905|4911|4936|6333|6759)[0-9]{14}|(4903|4905|4911|4936|6333|6759)[0-9]{15}|564182[0-9]{10}|564182[0-9]{12}|564182[0-9]{13}|633110[0-9]{10}|633110[0-9]{12}|633110[0-9]{13}$");
            put("UNION", "^(62[0-9]{14,17})$");
            put("VISAMASTERCARD", "^(?:4[0-9]{12}(?:[0-9]{3})?|5[1-5][0-9]{14})$");
        }
    };

    public static String maskCard(String input) {
        String mask = "%s%s%s";
        String wildCard = "******";
        input = input.replace("\n", "").replace("\r", "");
        input = input.replace("\\n", "");
        input = input.replace("\\", "");
        for (String pattern : CARDREGEXP.values()) {
            StringBuffer sb = new StringBuffer();
            Pattern r = Pattern.compile(pattern);
            Matcher m = r.matcher(input);

            while (m.find()) {
                try {
                    if (m.groupCount() == 0 && m.group(0).length() >= 4) {
                        if (m.group(0).length() > 10) {
                            m.appendReplacement(sb, String.format(mask, m.group(0).substring(0, 6), wildCard, m.group(0).substring(m.group(0).length() - 4)));
                        } else {
                            m.appendReplacement(sb, String.format(mask, wildCard, wildCard, m.group(0).substring(m.group(0).length() - 4)));
                        }
                    } else {
                        String card =  m.group(0) != null ? m.group(0).replaceAll("[^\\d.]", "") : null;
                        String bin = card != null && card.length() > 10 ? card.substring(0, 6) : "";
                        
                        m.appendReplacement(sb, m.group(1) + String.format(mask, bin, wildCard, m.group(2)) + m.group(3));
                    }
                } catch (Exception ex) {

                }
            }
            m.appendTail(sb);
            input = sb.toString();
        }

        return input;
    }

}

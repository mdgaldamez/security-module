/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import javax.enterprise.context.spi.Contextual;
import javax.enterprise.inject.spi.Bean;

/**
 *
 * @author Peter Galdamez
 */
public class ContextualUtils {

    public static <T> boolean isBean(Contextual<T> contextual) {
        return contextual instanceof Bean;
    }
}

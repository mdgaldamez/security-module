/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import java.io.IOException;
import java.util.Map;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdámez
 */
public class MapToArraySerializer extends JsonSerializer<Map<Object, Object>> {

    private static final Logger LOGGER = LoggerFactory.getLogger(MapToArraySerializer.class);
    
    private Class<?> keyAs;

    private Class<?> contentAs;

    @Override
    public void serialize(Map<Object, Object> map, JsonGenerator generator, SerializerProvider serializers) throws IOException {

        generator.writeStartArray();

        map.forEach((key, value) -> {
            try {
                generator.writeStartObject();
                generator.writeObjectField(key.toString(), value);
                generator.writeEndObject();
            } catch (IOException e) {
                LOGGER.error("Cannot serialize entry with key {} and value {}", key, value);
            }

        });

        generator.writeEndArray();
    }
}

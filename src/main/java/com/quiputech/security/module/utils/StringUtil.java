/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.utils;

import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.StringTokenizer;
import java.util.UUID;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.xml.bind.DatatypeConverter;

/**
 * Utility dealing with Strings
 * @author Peter Galdamez
 * 
 */
public class StringUtil
{
   private static final Pattern COMMAND_ARGS_PATTERN = Pattern.compile("([^'\"\\s]+)|\"([^\"\\\\]*(?:\\\\.[^\"\\\\]*)*)\"|\'([^'\\\\]*(?:\\\\.[^'\\\\]*)*)\'");
   public static final String PROPERTY_DEFAULT_SEPARATOR = "::";
	
   /**
    * Check whether the passed string is null or empty
    * @param str
    * @return
    */
   public static boolean isNotNull(String str)
   {
      return str != null && !"".equals(str.trim());
   }

   /**
    * Check whether the string is null or empty
    * @param str
    * @return
    */
   public static boolean isNullOrEmpty(String str)
   {
      return str == null || str.isEmpty();
   }

   /**
    * Match two strings else throw a {@link RuntimeException}
    * @param first
    * @param second
    */
   public static void match(String first, String second)
   {
      if (first.equals(second) == false)
         //throw BlackBoxMessages.MESSAGES.failedToMatchStrings(first, second);
          throw new RuntimeException("Failed to match Strings [" + first + "," + second + "]");
   }

   /**
    * Given a comma separated string, get the tokens as a {@link List}
    * @param str
    * @return
    */
   public static List<String> tokenize(String str)
   {
      List<String> list = new ArrayList<>();
      StringTokenizer tokenizer = new StringTokenizer(str, ",");
      while (tokenizer.hasMoreTokens())
      {
         list.add(tokenizer.nextToken());
      }
      return list;
   }

    /**
     * Transforms a string to a camel case representation, including the first
     * character.
     *
     * <p>
     * Examples:
     * <ul>
     * <li><tt>toCamelCase("hello world") -> "HelloWorld"</tt></li>
     * <li><tt>toCamelCase("hello_world") -> "HelloWorld"</tt></li>
     * <li><tt>toCamelCase("hello_World") -> "HelloWorld"</tt></li>
     * <li><tt>toCamelCase("helloWorld") -> "HelloWorld"</tt></li>
     * <li><tt>toCamelCase("HelloWorld") -> "HelloWorld"</tt></li>
     * </ul>
     *
     * @param str
     * @return
     */
    public static String toCamelCase(String str) {
        StringBuilder sb = new StringBuilder();

        for (String s : str.split("[-_ ]")) {
            if (s.length() > 0) {
                sb.append(Character.toUpperCase(s.charAt(0)));

                if (s.length() > 1) {
                    sb.append(s.substring(1, s.length()));
                }
            }
        }

        return sb.toString();
    }

    /**
     * Transforms a string to underscore-delimited representation.
     *
     * <p>
     * Examples:
     * <ul>
     * <li><tt>toUnderScoreDelimited("HelloWorld") -> "hello_world"</tt></li>
     * <li><tt>toUnderScoreDelimited("helloWorld") -> "hello_world"</tt></li>
     * </ul>
     *
     * @param str
     * @return
     */
    public static String toSnakeCase(String str) {
        StringBuilder sb = new StringBuilder();

        for (char c : str.toCharArray()) {
            if (Character.isLetter(c) || Character.isDigit(c)) {
                if (Character.isUpperCase(c)) {
                    if (sb.length() > 0) {
                        sb.append("_");
                    }

                    sb.append(Character.toLowerCase(c));
                } else {
                    sb.append(c);
                }
            } else {
                sb.append("_");
            }
        }

        return sb.toString();
    }

    /**
     * Converts a byte array to a lower case hex representation. If the given
     * byte array is <tt>null</tt>, an empty string is returned.
     *
     * @param bytes
     * @return
     */
    public static String toHex(byte[] bytes) {
        if (bytes == null) {
            return "";
        } else {
            return DatatypeConverter.printHexBinary(bytes).toLowerCase();
        }
    }

    /**
     * Creates byte array from a hex represented string.
     *
     * @param s
     * @return
     */
    public static byte[] fromHex(String s) {
        return DatatypeConverter.parseHexBinary(s); // fast!
    }

    /**
     * Creates a byte array from a given string, using the UTF-8 encoding. This
     * calls {@link String#getBytes(java.nio.charset.Charset)} internally with
     * "UTF-8" as charset.
     *
     * @param s
     * @return
     */
    public static byte[] toBytesUTF8(String s) {
        try {
            return s.getBytes("UTF-8");
        } catch (UnsupportedEncodingException e) {
            throw new RuntimeException("JVM does not support UTF-8 encoding.", e);
        }
    }

    /**
     * Returns the count of the substring
     *
     * @param haystack
     * @param needle
     * @return
     */
    public static int substrCount(String haystack, String needle) {
        int lastIndex = 0;
        int count = 0;

        if (needle != null && haystack != null) {
            while (lastIndex != -1) {
                lastIndex = haystack.indexOf(needle, lastIndex);

                if (lastIndex != -1) {
                    count++;
                    lastIndex += needle.length();
                }
            }
        }

        return count;
    }

    public static String getStackTrace(Exception exception) {
        StringWriter stackTraceStringWriter = new StringWriter();
        exception.printStackTrace(new PrintWriter(stackTraceStringWriter));

        return stackTraceStringWriter.toString();
    }

    public static <T> String join(List<T> objects, String delimiter, StringJoinListener<T> listener) {
        StringBuilder objectsStr = new StringBuilder();

        for (int i = 0; i < objects.size(); i++) {
            if (listener != null) {
                objectsStr.append(listener.getString(objects.get(i)));
            } else {
                objectsStr.append(objects.get(i).toString());
            }

            if (i < objects.size() - 1) {
                objectsStr.append(delimiter);
            }
        }

        return objectsStr.toString();
    }

    public static <T> String join(List<T> objects, String delimiter) {
        return join(objects, delimiter, null);
    }

    public static <T> String join(T[] objects, String delimiter, StringJoinListener<T> listener) {
        return join(Arrays.asList(objects), delimiter, listener);
    }

    public static <T> String join(T[] objects, String delimiter) {
        return join(Arrays.asList(objects), delimiter, null);
    }

    public static interface StringJoinListener<T> {

        public String getString(T object);
    }

    public static List<String> splitCommandLineArgs(String commandLine) {
        List<String> commandArgsList = new ArrayList<>();
        Matcher commandArgsMatcher = COMMAND_ARGS_PATTERN.matcher(commandLine);

        while (commandArgsMatcher.find()) {
            if (commandArgsMatcher.group(1) != null) {
                commandArgsList.add(commandArgsMatcher.group(1));
            } else if (commandArgsMatcher.group(2) != null) {
                commandArgsList.add(commandArgsMatcher.group(2).replace("\\", ""));
            } else if (commandArgsMatcher.group(3) != null) {
                commandArgsList.add(commandArgsMatcher.group(3).replace("\\", ""));
            }
        }

        return commandArgsList;
    }

    /**
     * Generate short UUID (13 characters)
     *
     * @return short UUID
     */
    public static String shortUUID() {
        UUID uuid = UUID.randomUUID();
        long l = ByteBuffer.wrap(uuid.toString().getBytes()).getLong();
        return Long.toString(l, Character.MAX_RADIX);
    }
}
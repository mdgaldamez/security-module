package com.quiputech.security.module.utils;

import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.net.URI;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import javax.ws.rs.core.MediaType;
import org.apache.http.HttpException;
import org.apache.http.NameValuePair;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Michael Galdámez
 */
public class Util {
    private static final Logger LOGGER = LoggerFactory.getLogger(Util.class);
    private static final ObjectMapper MAPPER = new ObjectMapper();
    
    public static String executePOSTRequest(String request, String URL, String accessToken) {

        try (CloseableHttpClient client = HttpClients.createDefault()) {
            String responseAsString = "";
            HttpPost post = new HttpPost(URL);
            LOGGER.debug(URL);
            
            StringEntity reqEntity = new StringEntity(request);
            post.setHeader("Content-Type", MediaType.APPLICATION_JSON);
            post.setHeader("Authorization", "bearer " + accessToken);
            post.setEntity(reqEntity);
            
            try (CloseableHttpResponse response = client.execute(post)) {

                int statusCode = response.getStatusLine().getStatusCode();
                String statusMessage = response.getStatusLine().getReasonPhrase();
                LOGGER.trace("Status: " + statusMessage);
                switch(statusCode) {
                    case 200:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.trace(responseAsString);
                    default:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.error(responseAsString);
                }
            }
            return responseAsString;
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        }
    }

    public static String getToken(String URL, String credential) throws IOException{
        try (CloseableHttpClient client = HttpClients.createDefault()) {
            LOGGER.debug(URL);
            Map<String, Object> map = MAPPER.readValue(credential, Map.class);
            HttpPost post = new HttpPost(URL);
            List<NameValuePair> nvps = new ArrayList <>();
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                String key = entry.getKey();
                String value = String.valueOf(entry.getValue());
                if(entry.getValue() != null) {
                    nvps.add(new BasicNameValuePair(key, value));
                }
            }
            post.setEntity(new UrlEncodedFormEntity(nvps, StandardCharsets.UTF_8));
            
            try (CloseableHttpResponse response = client.execute(post)) {
                int statusCode = response.getStatusLine().getStatusCode();
                String statusMessage = response.getStatusLine().getReasonPhrase();
                LOGGER.trace("Status: " + statusMessage);
                String responseAsString = "";
                switch(statusCode) {
                    case 200:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.trace(responseAsString);

                        return responseAsString;
                    default:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.error(responseAsString);
                }

                return responseAsString;
            }            
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        }
    }
    
    public static String executeGETRequest(String request, String URL, String accessToken) throws IOException{
        try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
            LOGGER.debug(URL);
            HttpGet get = new HttpGet(URL);
            get.addHeader("Authorization", "bearer " + accessToken);
            if(!request.isEmpty()){
                Map<String, Object> map = MAPPER.readValue(request, Map.class);               
                List<NameValuePair> nvps = new ArrayList <>();
                for (Map.Entry<String, Object> entry : map.entrySet()) {
                    String key = entry.getKey();
                    String value = String.valueOf(entry.getValue());
                    if(entry.getValue() != null) {
                        nvps.add(new BasicNameValuePair(key, value));
                    }
                }
                URI uri = new URIBuilder(get.getURI()).addParameters(nvps).build();
                get.setURI(uri);
            }

            try (CloseableHttpResponse httpresponse = httpclient.execute(get)) {
                String response = EntityUtils.toString(httpresponse.getEntity());
                LOGGER.info("Response: " + response);
                return response;
            }
        }catch(Exception ex) {            
            return null;
        }
    }
    
    public static String executeDELETERequest(String URL, String accessToken) throws IOException, HttpException {
         try (CloseableHttpClient httpclient = HttpClients.createDefault()) {
                String responseAsString = "";
                HttpDelete httpDelete = new HttpDelete(URL);
                httpDelete.setHeader("Accept", MediaType.APPLICATION_JSON);
                httpDelete.setHeader("Authorization", "bearer " + accessToken);

                System.out.println("Executing request " + httpDelete.getRequestLine());
                try (CloseableHttpResponse httpresponse = httpclient.execute(httpDelete)) {
                    int statusCode = httpresponse.getStatusLine().getStatusCode();
                    String statusMessage = httpresponse.getStatusLine().getReasonPhrase();
                    LOGGER.trace("Status: " + statusMessage);
                    switch(statusCode) {
                        case 200:
                            responseAsString = EntityUtils.toString(httpresponse.getEntity());
                            LOGGER.trace(responseAsString);
                            break;
                        case 204:
                            break;
                        default:
                            responseAsString = EntityUtils.toString(httpresponse.getEntity());
                            LOGGER.error(responseAsString);
                            break;
                    }
                    httpresponse.close();                    
                    }
                httpclient.close();
                return responseAsString;
        }catch(Exception ex) {            
            return null;
        }
    }
    
    public static String executePUTRequest(String request, String URL, String accessToken) {

        try (CloseableHttpClient client = HttpClients.createDefault()) {
            String responseAsString = "";
            HttpPut httpPut = new HttpPut(URL);
            LOGGER.debug(URL);

            StringEntity entity = new StringEntity(request, "UTF-8");
            entity.setContentType(MediaType.APPLICATION_JSON);
            httpPut.setEntity(entity);
            httpPut.setHeader("Authorization", "bearer " + accessToken);
            
            try (CloseableHttpResponse response = client.execute(httpPut)) {

                int statusCode = response.getStatusLine().getStatusCode();
                String statusMessage = response.getStatusLine().getReasonPhrase();
                LOGGER.trace("Status: " + statusMessage);
                switch(statusCode) {
                    case 200:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.trace(responseAsString);
                        break;
                    case 204:
                        break;
                    default:
                        responseAsString = EntityUtils.toString(response.getEntity());
                        LOGGER.error(responseAsString);
                }
            }
            return responseAsString;
        } catch (IOException e) {
            LOGGER.error("Was unable to send PUT request:" + request + " (displaying first 1000 chars) from url-" + URL, e.getMessage());
            return null;
        }
    }
}

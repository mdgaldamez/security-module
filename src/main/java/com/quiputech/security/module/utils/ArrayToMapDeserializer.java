package com.quiputech.security.module.utils;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.ObjectCodec;
import com.fasterxml.jackson.databind.BeanProperty;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.deser.ContextualDeserializer;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author Michael Galdámez
 */
public class ArrayToMapDeserializer extends JsonDeserializer<Map<Object, Object>>
    implements ContextualDeserializer {

    private Class<?> keyAs;

    private Class<?> contentAs;
    

    @Override
    public Map<Object, Object> deserialize(JsonParser p, DeserializationContext ctxt)
        throws IOException, JsonProcessingException {
        return this.deserialize(p, ctxt, new HashMap<>());
    }
    
/*
    @Override
    public Map<Object, Object> deserialize(JsonParser p, DeserializationContext ctxt, Map<Object, Object> intoValue) throws IOException, JsonProcessingException {
        JsonNode node = p.readValueAsTree();
        ObjectCodec codec = p.getCodec();
        if (node.isArray()) {
            node.forEach(entry -> {
                try {
                    JsonNode keyNode = entry.get(0);
                    JsonNode valueNode = entry.get(1);
                    intoValue.put(keyNode.traverse(codec).readValueAs(this.keyAs),
                        valueNode.traverse(codec).readValueAs(this.contentAs));
                } catch (NullPointerException | IOException e) {
                    // skip entry
                }
            });
        }
        return intoValue;
    }
*/
    @Override
    public Map<Object, Object> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext, Map<Object, Object> map)
        throws IOException, JsonProcessingException {
        //Map<Object, Object> map = null;

        // Skip START_ARRAY and set the pointer in the first element of the array.
        jsonParser.nextValue();
        int length = jsonParser.getIntValue();

        if(length >= 0) {
            map = new HashMap<>();
            // Set the pointer in the second element of the array
            jsonParser.nextValue();
            for (int i = 0; i < length; i++) {
                Object key = jsonParser.readValueAs(keyAs);
                Object value = jsonParser.readValueAs(contentAs);
                map.put(key, value);
            }
        }
        // We have to skip the END_ARRAY which wraps the list of elements, in order to return a parser with a pointer in a consistent position
        jsonParser.nextValue();
        return map;
    }
    
    @Override
    public JsonDeserializer<?> createContextual(DeserializationContext ctxt, BeanProperty property)
        throws JsonMappingException {
        JsonDeserialize jsonDeserialize = property.getAnnotation(JsonDeserialize.class);
        this.keyAs = jsonDeserialize.keyAs();
        this.contentAs = jsonDeserialize.contentAs();
        return this;
    }
}

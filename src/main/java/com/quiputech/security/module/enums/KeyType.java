/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.enums;

/**
 *
 * @author Peter Galdamez
 */
public enum KeyType {
    WRAPPED("WRAPPED"),
    EXPOSED("EXPOSED");

    private final String value;

    private KeyType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static KeyType parse(String s) {
        if (s != null) {
            s = s.trim().toLowerCase();
            if (s.equals(EXPOSED.toString().toLowerCase())) {
                return EXPOSED;
            } else if (s.equals(WRAPPED.toString().toLowerCase())) {
                return WRAPPED;
            }
        } 
        
        return EXPOSED;
    }
}


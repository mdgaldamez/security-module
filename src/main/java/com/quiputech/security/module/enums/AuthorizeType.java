/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.enums;

/**
 *
 * @author peter
 */
public enum AuthorizeType {
    ALL("ALL"),
    WHITELIST("WHITELIST");

    private final String value;

    private AuthorizeType(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    public static AuthorizeType parse(String s) {
        if (s != null) {
            s = s.trim().toLowerCase();
            if (s.equals(ALL.toString().toLowerCase())) {
                return ALL;
            } else if (s.equals(WHITELIST.toString().toLowerCase())) {
                return WHITELIST;
            }
        } 
        
        return WHITELIST;
    }
}

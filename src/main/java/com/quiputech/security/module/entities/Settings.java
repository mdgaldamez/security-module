package com.quiputech.security.module.entities;

import com.fasterxml.jackson.annotation.JsonProperty;

/**
 * @author mgaldamez
 */
public class Settings {
    private String appId;
    private String clientId;
    private String poold;
    private String region;
    private String realm;
    private String clientSecret;
    private String username;
    private String password;
    private int mode;
    private int sendEmail;

    public String getUsername() {
        if (username == null){
            username = "";
        }
        return username;
    }
    
    @JsonProperty("username")
    public void setUsername(String username) {
        this.username = username;
    }

    public String getClientSecret() {
        if (clientSecret == null){
            clientSecret = "";
        }
        return clientSecret;
    }

    @JsonProperty("clientSecret")
    public void setClientSecret(String clientSecret) {
        this.clientSecret = clientSecret;
    }

    public String getClientId() {
        if (clientId == null){
            clientId = "";
        }
        return clientId;
    }

    @JsonProperty("clientId")
    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public String getPoold() {
        if (poold == null){
            poold = "";
        }
        return poold;
    }

    @JsonProperty("poold")
    public void setPoold(String poold) {
        this.poold = poold;
    }

    public String getRegion() {
        if (region == null){
            region = "";
        }
        return region;
    }

    @JsonProperty("region")
    public void setRegion(String region) {
        this.region = region;
    }

    public String getRealm() {
        if (realm == null){
            realm = "";
        }
        return realm;
    }

    @JsonProperty("realm")
    public void setRealm(String realm) {
        this.realm = realm;
    }
    
    public String getPassword() {
        if (password == null){
            password = "";
        }
        return password;
    }

    @JsonProperty("password")
    public void setPassword(String password) {
        this.password = password;
    }
    
    public int getMode() {
        return mode;
    }
    
    @JsonProperty("mode")
    public void setMode(int mode) {
        this.mode = mode;
    }
    
    public String getAppId() {
        return appId;
    }

    @JsonProperty("appId")
    public void setAppId(String appId) {
        this.appId = appId;
    }

    public int getSendEmail() {
        return sendEmail;
    }

    @JsonProperty("sendEmail")
    public void setSendEmail(int sendEmail) {
        this.sendEmail = sendEmail;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;

import java.util.HashMap;

/**
 *
 * @author Michael Galdámez
 */
public class PrincipalCollection {
    
    private Object principal;
    HashMap<String, Object> principalCollection = new HashMap<>();

    public Object getPrincipal() {
        return principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }
    
    public HashMap<String, Object> getPrincipalCollection() {
        return principalCollection;
    }

    public void setPrincipalCollection(HashMap<String, Object> principalCollection) {
        this.principalCollection = principalCollection;
    }
}

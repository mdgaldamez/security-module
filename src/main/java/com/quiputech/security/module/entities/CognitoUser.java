/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;

import java.util.Date;
import java.util.HashMap;

/**
 *
 * @author Michael Galdámez
 */
public class CognitoUser {
    private HashMap<String, String> attributes;
    private Boolean enabled;
    private HashMap<String, String> mfaOptions;
    private Date userCreateDate;
    private Date userLastModifiedDate;
    private String username;
    private String userStatus;

    public HashMap<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }

    public Boolean getEnabled() {
        return enabled;
    }

    public void setEnabled(Boolean enabled) {
        this.enabled = enabled;
    }

    public HashMap<String, String> getMfaOptions() {
        return mfaOptions;
    }

    public void setMfaOptions(HashMap<String, String> mfaOptions) {
        this.mfaOptions = mfaOptions;
    }

    public Date getUserCreateDate() {
        return userCreateDate;
    }

    public void setUserCreateDate(Date userCreateDate) {
        this.userCreateDate = userCreateDate;
    }

    public Date getUserLastModifiedDate() {
        return userLastModifiedDate;
    }

    public void setUserLastModifiedDate(Date userLastModifiedDate) {
        this.userLastModifiedDate = userLastModifiedDate;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserStatus() {
        return userStatus;
    }

    public void setUserStatus(String userStatus) {
        this.userStatus = userStatus;
    }
}

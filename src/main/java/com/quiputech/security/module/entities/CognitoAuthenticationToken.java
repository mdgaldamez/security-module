package com.quiputech.security.module.entities;

/**
 *
 * @author mgaldamez
 */
public class CognitoAuthenticationToken {
    private Object principal;
    private Object credentials;

    public Object getPrincipal() {
        return principal;
    }

    public void setPrincipal(Object principal) {
        this.principal = principal;
    }

    public Object getCredentials() {
        return credentials;
    }

    public void setCredentials(Object credentials) {
        this.credentials = credentials;
    }    
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;

import java.util.List;

/**
 *
 * @author Michael Galdámez
 */
public class UserConsentRepresentation {
    private String clientId;
    private List<String> grantedClientScopes;
    private Long createdDate;
    private Long lastUpdatedDate;

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public List<String> getGrantedClientScopes() {
        return grantedClientScopes;
    }

    public void setGrantedClientScopes(List<String> grantedClientScopes) {
        this.grantedClientScopes = grantedClientScopes;
    }

    public Long getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Long createdDate) {
        this.createdDate = createdDate;
    }

    public Long getLastUpdatedDate() {
        return lastUpdatedDate;
    }

    public void setLastUpdatedDate(Long lastUpdatedDate) {
        this.lastUpdatedDate = lastUpdatedDate;
    }
    
}

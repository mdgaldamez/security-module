/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;


/**
 *
 * @author Michael
 */
public class CognitoAuthenticationInfo {
    
    private CognitoAuthenticationToken authentication;
    private String accessToken;
    private PrincipalCollection principals;
    
    public CognitoAuthenticationInfo (CognitoAuthenticationToken auth, String token) {
        this.authentication = auth;
        this.accessToken = token;
    }
    
    public CognitoAuthenticationInfo (PrincipalCollection principals, String token) {
        this.principals = principals;
        this.accessToken = token;
    }

    public CognitoAuthenticationToken getAuthentication() {
        return authentication;
    }

    public void setAuthentication(CognitoAuthenticationToken authentication) {
        this.authentication = authentication;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String token) {
        this.accessToken = token;
    }
    
    public PrincipalCollection getPrincipals() {
        return principals;
    }

    public void setPrincipals(PrincipalCollection principals) {
        this.principals = principals;
    }
}

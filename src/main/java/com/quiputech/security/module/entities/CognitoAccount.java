package com.quiputech.security.module.entities;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author mgaldamez
 */
public class CognitoAccount  {
    public static final String ACQUIRER = "acquirer";
    public static final String MERCHANT = "merchant";
    public static final String COMPANY = "company";
    public static final String MERCHANTRUC = "merchantRUC";
    
    private String email;
    private String refreshToken;
    private String accessToken;
    private List<String> groups;
    private Map<String, String> attributes;

    public CognitoAccount() {}
    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
     * @return the refreshToken
     */
    public String getRefreshToken() {
        return refreshToken;
    }

    /**
     * @param refreshToken the refreshToken to set
     */
    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }

    public String getAccessToken() {
        if (accessToken == null)
            accessToken = "";
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }
    
    public List<String> getGroups() {
        if (groups == null)
            groups = new ArrayList<>();
        return groups;
    }

    public void setGroups(List<String> group) {
        this.groups = group;
    }

    public Map<String, String> getAttributes() {
        if (attributes == null)
            attributes = new LinkedHashMap<>();
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }
    
    /*public String getGroup() {
        for (String s : getGroups()) {
            if (s.equalsIgnoreCase(ACQUIRER)) {
                return ACQUIRER;
            } else if (s.equalsIgnoreCase(MERCHANT)) {
                return MERCHANT;
            } else if (s.equalsIgnoreCase(COMPANY)) {
                return COMPANY;
            } else if (s.equalsIgnoreCase(MERCHANTRUC)){
               return MERCHANTRUC;     
            }
        }
        
        return "";
    }*/
    
}

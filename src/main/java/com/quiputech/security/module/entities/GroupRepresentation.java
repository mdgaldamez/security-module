/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;

import java.util.HashMap;

/**
 *
 * @author Michael Galdámez
 */
public class GroupRepresentation {
    private String id;
    private String name;
    private String path;
    private HashMap<String, String> attributes;
    private String[] realmRoles;
    private HashMap<String, String> clientRoles;
    private GroupRepresentation[] subGroup;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public HashMap<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }

    public String[] getRealmRoles() {
        return realmRoles;
    }

    public void setRealmRoles(String[] realmRoles) {
        this.realmRoles = realmRoles;
    }

    public HashMap<String, String> getClientRoles() {
        return clientRoles;
    }

    public void setClientRoles(HashMap<String, String> clientRoles) {
        this.clientRoles = clientRoles;
    }

    public GroupRepresentation[] getSubGroup() {
        return subGroup;
    }

    public void setSubGroup(GroupRepresentation[] subGroup) {
        this.subGroup = subGroup;
    }
    
}

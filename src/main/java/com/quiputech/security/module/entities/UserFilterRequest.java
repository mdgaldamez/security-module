package com.quiputech.security.module.entities;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.util.HashMap;

/**
 *
 * @author mgaldamez
 */
@JsonIgnoreProperties(ignoreUnknown = true)
public class UserFilterRequest {
    private String attribute_name;
    private String filter_type;
    private String attribute_value;
    private String[] attributesToGet;
    private HashMap<String, String> attributes;

    public HashMap<String, String> getAttributes() {
        return attributes;
    }

    public void setAttributes(HashMap<String, String> attributes) {
        this.attributes = attributes;
    }

    public String[] getAttributesToGet() {
        return attributesToGet;
    }

    public void setAttributesToGet(String[] attributesToGet) {
        this.attributesToGet = attributesToGet;
    }
    
    public String getFilter_type() {
        return filter_type;
    }

    public void setFilter_type(String filter_type) {
        this.filter_type = filter_type;
    }

    public String getAttribute_value() {
        return attribute_value;
    }

    public void setAttribute_value(String attribute_value) {
        this.attribute_value = attribute_value;
    }

    public String getAttribute_name() {
        return attribute_name;
    }

    public void setAttribute_name(String attribute_name) {
        this.attribute_name = attribute_name;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.entities;

/**
 *
 * @author Michael Galdámez
 */
public class Attributes {
    private UserRepresentation[] userRepresentation;

    public UserRepresentation[] getUserRepresentation() {
        return userRepresentation;
    }

    public void setUserRepresentation(UserRepresentation[] userRepresentation) {
        this.userRepresentation = userRepresentation;
    }
}

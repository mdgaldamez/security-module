/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.crypto;
import com.quiputech.security.module.utils.StringUtil;
import javax.crypto.SecretKey;
import java.util.Arrays;

/**
 * A salted secret key is a convenience class to bundle a {@link SecretKey} with
 * its corresponding salt. It is mainly used to represent the master key and the
 * master key salt.
 *
 * @author Philipp C. Heckel <philipp.heckel@gmail.com>
 */
public class SaltedSecretKey implements SecretKey {
    private static final long serialVersionUID = 1216126055360327470L;

    private final SecretKey secretKey;
    private final byte[] salt;

    public SaltedSecretKey(SecretKey secretKey, byte[] salt) {
        this.secretKey = secretKey;
        this.salt = salt;
    }

    public byte[] getSalt() {
        return salt;
    }

    @Override
    public String getAlgorithm() {
        return secretKey.getAlgorithm();
    }

    @Override
    public String getFormat() {
        return secretKey.getFormat();
    }

    @Override
    public byte[] getEncoded() {
        return secretKey.getEncoded();
    }

    @Override
    public String toString() {
        return "[secretKey={algorithm=" + getAlgorithm() + ", format=" + getFormat() + ", encoded=" + StringUtil.toHex(getEncoded()) + "}, salt="
                + StringUtil.toHex(getSalt()) + "]";
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + Arrays.hashCode(salt);
        result = prime * result + ((secretKey == null) ? 0 : secretKey.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (!(obj instanceof SaltedSecretKey)) {
            return false;
        }
        SaltedSecretKey other = (SaltedSecretKey) obj;
        if (!Arrays.equals(salt, other.salt)) {
            return false;
        }
        if (secretKey == null) {
            if (other.secretKey != null) {
                return false;
            }
        } else if (!secretKey.equals(other.secretKey)) {
            return false;
        }
        return true;
    }
}

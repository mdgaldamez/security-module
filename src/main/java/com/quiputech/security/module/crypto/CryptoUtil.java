package com.quiputech.security.module.crypto;

import com.quiputech.security.module.utils.StringUtil;
import java.io.UnsupportedEncodingException;
import java.security.AlgorithmParameters;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.InvalidParameterSpecException;
import java.security.spec.KeySpec;
import java.util.Base64;
import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.PBEKeySpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.DecoderException;
import org.apache.commons.codec.binary.Hex;
import static java.nio.charset.StandardCharsets.ISO_8859_1;
import java.security.NoSuchProviderException;
import javax.crypto.KeyGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.bouncycastle.crypto.Digest;
import org.bouncycastle.crypto.digests.SHA256Digest;
import org.bouncycastle.crypto.generators.HKDFBytesGenerator;
import org.bouncycastle.crypto.params.HKDFParameters;

/**
 *
 * @author Peter Galdamez
 */
public class CryptoUtil {

    private static final Logger LOGGER = LoggerFactory.getLogger(CryptoUtil.class);
    private static final byte[] KEY_DERIVATION_INFO = StringUtil.toBytesUTF8("Syncany_SHA256_Derivated_Key");
    private static final String KEY_DERIVATION_FUNCTION = "PBKDF2WithHmacSHA256";
    private static final Digest KEY_DERIVATION_DIGEST = new SHA256Digest();
    private static final SecureRandom SECURE_RANDOM = new SecureRandom();
    private static final String ALGORITHM = "AES/CBC/PKCS5Padding";
    private static final int AES_KEY_SIZE = 256;
    private static final int MASTER_KEY_SIZE = 128;
    private static final String AES = "AES";
    private static final String UTF8 = "UTF-8";
    private static final int IV_SIZE = 16;

    /**
     *
     * @param size
     * @return
     */
    public static byte[] createRandomArray(int size) {
        byte[] randomByteArray = new byte[size];
        SECURE_RANDOM.nextBytes(randomByteArray);

        return randomByteArray;
    }

    /**
     *
     * @return
     */
    public static String generateIv() {
        return StringUtil.toHex(createRandomArray(IV_SIZE));
    }

    /**
     * Default encription AES
     *
     * @param passwordAsString
     * @param message
     * @param salt
     * @param iv
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidParameterSpecException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws UnsupportedEncodingException
     * @throws java.security.InvalidAlgorithmParameterException
     */
    public static String encrypt(String passwordAsString, String message, byte[] salt, byte[] iv) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        String[] result = encryptMessage(message, passwordAsString, salt, iv);
        return result.length >= 2 ? result[2] : null;
    }

    /**
     * Default deryption AES
     *
     * @param passwordAsString
     * @param message
     * @param salt
     * @param iv
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidParameterSpecException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws UnsupportedEncodingException
     * @throws java.security.InvalidAlgorithmParameterException
     */
    public static String decrypt(String passwordAsString, String message, byte[] salt, byte[] iv) throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {
        return decryptMessage(message, passwordAsString, salt, iv);
    }

    /**
     * Decrypt Message using AES
     *
     * @param cipherAsString
     * @param passwordAsString
     * @param salt
     * @param iv
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws UnsupportedEncodingException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private static String decryptMessage(String cipherAsString, String passwordAsString, byte[] salt, byte[] iv)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {

        byte[] cipherText = Base64.getDecoder().decode(cipherAsString);

        SecretKey secret = createSecretKey(passwordAsString, salt);

        /* Decrypt the message, given derived key and initialization vector. */
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secret, new IvParameterSpec(iv));
        return new String(cipher.doFinal(cipherText), UTF8);
    }

    /**
     * Decrypt Message using AES
     *
     * @param cipherAsString
     * @param passwordAsString
     * @param ivAsString
     * @param saltAsString
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws UnsupportedEncodingException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    public static String decrypt(String cipherAsString, String passwordAsString, String ivAsString, String saltAsString)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException {
        try {
            byte[] salt = saltAsString != null ? StringUtil.fromHex(saltAsString) : null;
            byte[] iv = StringUtil.fromHex(ivAsString);
            return decryptMessage(cipherAsString, passwordAsString, salt, iv);
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }

        return null;
    }

    /**
     * Decrypt Message using AES
     *
     * @param cipherAsString
     * @param passwordAsString
     * @param ivAsString
     * @param saltAsString
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidAlgorithmParameterException
     * @throws UnsupportedEncodingException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws java.security.spec.InvalidParameterSpecException
     */
    public static String encrypt(String cipherAsString, String passwordAsString, String ivAsString, String saltAsString)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidAlgorithmParameterException, UnsupportedEncodingException, BadPaddingException, IllegalBlockSizeException, InvalidParameterSpecException {

        byte[] salt = saltAsString != null ? StringUtil.fromHex(saltAsString) : null;
        byte[] iv = StringUtil.fromHex(ivAsString);

        String[] result = encryptMessage(cipherAsString, passwordAsString, salt, iv);
        return result.length >= 2 ? result[2] : null;
    }

    /**
     * Default encription AES
     *
     * @param message
     * @param passwordAsString
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidParameterSpecException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws UnsupportedEncodingException
     */
    public static String[] encryptMessage(String message, String passwordAsString)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        /* Generate salt */
        byte[] salt = SecureRandom.getInstanceStrong().generateSeed(8);
        return encryptMessage(message, passwordAsString, salt, StringUtil.fromHex(generateIv()));
    }

    /**
     * Default encription AES
     *
     * @param message
     * @param passwordAsString
     * @param salt
     * @param iv
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     * @throws NoSuchPaddingException
     * @throws InvalidKeyException
     * @throws InvalidParameterSpecException
     * @throws IllegalBlockSizeException
     * @throws BadPaddingException
     * @throws UnsupportedEncodingException
     * @throws java.security.InvalidAlgorithmParameterException
     */
    public static String[] encryptMessage(String message, String passwordAsString, byte[] salt, byte[] iv)
            throws NoSuchAlgorithmException, InvalidKeySpecException, NoSuchPaddingException, InvalidKeyException, InvalidParameterSpecException, IllegalBlockSizeException, BadPaddingException, UnsupportedEncodingException, InvalidAlgorithmParameterException {

        SecretKey secret = createSecretKey(passwordAsString, salt);

        /* Encrypt the message. */
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secret, new IvParameterSpec(iv));
        AlgorithmParameters params = cipher.getParameters();

        iv = params.getParameterSpec(IvParameterSpec.class).getIV();
        byte[] ciphertext = cipher.doFinal(message.getBytes(UTF8));

        // return generated values
        String saltAsString = salt != null ? Base64.getEncoder().encodeToString(salt) : "";
        String ivAsString = Base64.getEncoder().encodeToString(iv);
        String cipherTextAsString = Base64.getEncoder().encodeToString(ciphertext);
        return new String[]{saltAsString, ivAsString, cipherTextAsString};
    }

    /**
     *
     * @param keyString
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey createSecretKey(String keyString) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKey key = null;
        if (keyString != null) {
            key = createSecretKey(keyString, false);
        }
        return key;
    }

    /**
     *
     * @param keyString
     * @param randomSalt
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey createSecretKey(String keyString, boolean randomSalt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKey key = null;
        if (keyString != null) {
            /* Generate salt */
            byte[] salt = randomSalt ? SecureRandom.getInstanceStrong().generateSeed(8) : null;
            key = createSecretKey(keyString, salt);
        }
        return key;
    }

    /**
     *
     * @param keyString
     * @param salt
     * @return
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeySpecException
     */
    public static SecretKey createSecretKey(String keyString, byte[] salt) throws NoSuchAlgorithmException, InvalidKeySpecException {
        SecretKey key = null;
        if (keyString != null) {
            /* Generate salt */
            char[] password = keyString.toCharArray();

            if (salt != null) {
                /* Derive the key, given password and salt. */
                SecretKeyFactory factory = SecretKeyFactory.getInstance(KEY_DERIVATION_FUNCTION);
                KeySpec spec = new PBEKeySpec(password, salt, 65536, 256);
                SecretKey tmp = factory.generateSecret(spec);
                key = new SecretKeySpec(tmp.getEncoded(), AES);
            } else {
                key = new SecretKeySpec(hexStringToByteArray(keyString), AES);
            }
        }
        return key;
    }

    /**
     *
     * @param s
     * @return
     */
    public static byte[] hexStringToByteArray(String s) {
        int len = s.length();
        byte[] data = new byte[len / 2];
        for (int i = 0; i < len; i += 2) {
            data[i / 2] = (byte) ((Character.digit(s.charAt(i), 16) << 4)
                    + Character.digit(s.charAt(i + 1), 16));
        }
        return data;
    }

    /**
     *
     * @param str
     * @return
     */
    public static String convertStringToHex(String str) {

        char[] chars = str.toCharArray();

        StringBuilder hex = new StringBuilder();
        for (int i = 0; i < chars.length; i++) {
            hex.append(Integer.toHexString((int) chars[i]));
        }

        return hex.toString();
    }

    /**
     *
     * @param hex
     * @return
     * @throws DecoderException
     */
    public static String convertHexToString(String hex) throws DecoderException {
        return new String(Hex.decodeHex(hex.toCharArray()), ISO_8859_1);
    }

    /**
     *
     * @param secretKeyBytes
     * @param algorithm
     * @return
     */
    public static SecretKey toSecretKey(byte[] secretKeyBytes, String algorithm) {
        String plainAlgorithm = (algorithm.indexOf('/') != -1) ? algorithm.substring(0, algorithm.indexOf('/')) : algorithm;
        SecretKey secretKey = new SecretKeySpec(secretKeyBytes, plainAlgorithm);

        return secretKey;
    }

    /**
     *
     * @param secretKeyBytes
     * @param saltBytes
     * @param algorithm
     * @return
     */
    public static SaltedSecretKey toSaltedSecretKey(byte[] secretKeyBytes, byte[] saltBytes, String algorithm) {
        return new SaltedSecretKey(toSecretKey(secretKeyBytes, algorithm), saltBytes);
    }

    /**
     *
     * @param inputKeyMaterial
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static SaltedSecretKey createDerivedKey(String inputKeyMaterial)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {
        return createDerivedKey(StringUtil.fromHex(inputKeyMaterial), createRandomArray(AES_KEY_SIZE / 8), ALGORITHM, AES_KEY_SIZE);
    }

    /**
     *
     * @param inputKeyMaterial
     * @param inputSalt
     * @param outputKeyAlgorithm
     * @param outputKeySize
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static SaltedSecretKey createDerivedKey(byte[] inputKeyMaterial, byte[] inputSalt, String outputKeyAlgorithm, int outputKeySize)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

        HKDFBytesGenerator hkdf = new HKDFBytesGenerator(KEY_DERIVATION_DIGEST); //SHA-256
        hkdf.init(new HKDFParameters(inputKeyMaterial, inputSalt, KEY_DERIVATION_INFO)); //Additional Info

        byte[] derivedKey = new byte[outputKeySize / 8];
        hkdf.generateBytes(derivedKey, 0, derivedKey.length);

        return toSaltedSecretKey(derivedKey, inputSalt, outputKeyAlgorithm);
    }

    /**
     *
     * @param inputKeyMaterial
     * @param inputSalt
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static SaltedSecretKey createDerivedKey(String inputKeyMaterial, String inputSalt)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

        return createDerivedKey(StringUtil.fromHex(inputKeyMaterial), StringUtil.fromHex(inputSalt), ALGORITHM, MASTER_KEY_SIZE);
    }
    
     /**
     *
     * @param inputKeyMaterial
     * @param inputSalt
     * @return
     * @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static SaltedSecretKey create256DerivedKey(String inputKeyMaterial, String inputSalt)
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

        return createDerivedKey(StringUtil.fromHex(inputKeyMaterial), StringUtil.fromHex(inputSalt), ALGORITHM, AES_KEY_SIZE);
    }

    /**
     *
     * @return @throws InvalidKeySpecException
     * @throws NoSuchAlgorithmException
     * @throws NoSuchProviderException
     */
    public static String createKey()
            throws InvalidKeySpecException, NoSuchAlgorithmException, NoSuchProviderException {

        KeyGenerator keyGen = KeyGenerator.getInstance(AES);
        keyGen.init(AES_KEY_SIZE);
        SecretKey secretKey = keyGen.generateKey();

        return StringUtil.toHex(secretKey.getEncoded());
    }
}

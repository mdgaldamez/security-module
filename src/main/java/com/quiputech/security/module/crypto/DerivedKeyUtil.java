/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.crypto;

import com.quiputech.security.module.dto.SecurityKey;
import com.quiputech.security.module.enums.KeyType;
import com.quiputech.security.module.utils.StringUtil;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Peter Galdamez
 */
public class DerivedKeyUtil {
    private static final Logger LOGGER = LoggerFactory.getLogger(DerivedKeyUtil.class);
    
    public static SecurityKey createSecurityKey(KeyType keyType) {
        SecurityKey key = null;
        try {
            String masterKey = CryptoUtil.createKey();
            SaltedSecretKey  saltedKey = CryptoUtil.createDerivedKey(masterKey);
            
            String derived = StringUtil.toHex(saltedKey.getEncoded());
            String salt = StringUtil.toHex(saltedKey.getSalt());
            String iv = CryptoUtil.generateIv();
            
            if (derived != null) {
                key = new SecurityKey(masterKey, derived, salt, iv, keyType);
            }
        } catch(Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
        
        return key;
    }
}

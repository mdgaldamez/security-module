package com.quiputech.security.module;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import com.quiputech.security.module.entities.*;
import java.text.ParseException;

public class DatabaseManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(DatabaseManager.class);
    private static final String DATASOURCE_JNDI = "java:/ecorev3DS";
    private static ObjectMapper OBJ = new ObjectMapper(); 
    private static final String SELECT_SETTINGS_BY_CLIENT_ID = "SELECT * FROM ecorev3.POOL_MAPPING WHERE appId = ?";
    private static final String SELECT_USER = "SELECT * FROM ecorev3.USERS WHERE appId = ? AND username = ?";
    
    private static final String INSERT_USER = "INSERT INTO `ecorev3`.`USERS` (`appId`, `username`, `target`, `firstName`, `lastName`, "
                                            + "`email`, `status`, `json`, `creationDate`, `updateDate`) "
                                            + "VALUES (?, ?, ?, ?, ?, ?, ?, ?, sysdate(), sysdate());";
   
    private static final String QUERY_USER = "SELECT count(*) AS RESULT FROM ecorev3.USERS WHERE appId = ? AND username = ?";
    private static final String DELETE_USER = "DELETE FROM ecorev3.USERS WHERE appId = ? AND username = ?";
    
    private static final String UPDATE_USER = "UPDATE `ecorev3`.`USERS` SET `target` = ?, `firstName` = ?, `lastName` = ?, `email` = ?, "
                                            + "`status` = ?, `json` = ?, `updateDate` = sysdate() WHERE `appId` = ? AND `username` = ?";
    
    private Connection getConnection() throws NamingException, SQLException {
        InitialContext ic = new InitialContext();
        DataSource ds = (DataSource) ic.lookup(DATASOURCE_JNDI);
        return ds.getConnection();
    }
    
    public Settings getSettingsByClientId(String appId) {

        LOGGER.trace("Looking up " + appId);

        Connection conx = null;
        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(SELECT_SETTINGS_BY_CLIENT_ID);
            stmt.setString(1, appId);
            Settings setting;
            
            try (ResultSet rs = stmt.executeQuery()) {
                setting = new Settings();
                while (rs.next()) {
                    setting.setAppId(rs.getString("appId"));
                    setting.setClientId(rs.getString("clientId"));
                    setting.setClientSecret(rs.getString("clientSecret"));
                    setting.setPassword(rs.getString("password"));
                    setting.setPoold(rs.getString("poolId"));
                    setting.setRealm(rs.getString("realm"));
                    setting.setRegion(rs.getString("region"));
                    setting.setMode(rs.getInt("mode"));
                    setting.setUsername(rs.getString("username"));
                    setting.setSendEmail(rs.getInt("sendEmail"));
                }
            }
            return setting;
        } catch (SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        } finally {
            try {
                if (conx != null && !conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    public long queryUser(String appId, String username) {
        LOGGER.trace("Looking up " + username);

        Connection conx = null;
        long result = 0;
        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(QUERY_USER);
            stmt.setString(1, appId);
            stmt.setString(2, username);
            
            try (ResultSet rs = stmt.executeQuery()) {
                while (rs.next()) {
                    result = rs.getLong("RESULT");
                }
                return result;
            }
        } catch (SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return result;
        } finally {
            try {
                if (conx != null && !conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
    }
            
    public User getUser(String appId, String username) {
        LOGGER.trace("Looking up " + username);

        Connection conx = null;
        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(SELECT_USER);
            stmt.setString(1, appId);
            stmt.setString(2, username);
            User user;
            
            try (ResultSet rs = stmt.executeQuery()) {
                user = new User();
                while (rs.next()) {
                    user.setAppId(rs.getString("appId"));
                    user.setCreationDate(rs.getDate("creationDate"));
                    user.setEmail(rs.getString("email"));
                    user.setFirstName(rs.getString("firstName"));
                    user.setJson(rs.getString("json"));
                    user.setLastName(rs.getString("lastName"));
                    user.setStatus(rs.getString("status"));
                    user.setTarget(rs.getString("target"));
                    user.setUpdateDate(rs.getDate("updateDate"));
                    user.setUsername(rs.getString("username"));
                }
            }
            return user;
        } catch (SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return null;
        } finally {
            try {
                if (conx != null && !conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
    }
    
    public long insertUser(String appId, String target, UserRequest user) {

        LOGGER.trace("INSERT USER: " + user.getUsername());
        int result = 0;
        Connection conx = null;

        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(INSERT_USER);
            stmt.setObject(1, appId);
            stmt.setObject(2, user.getUsername());
            stmt.setObject(3, target);
            stmt.setObject(4, user.getName());
            stmt.setObject(5, user.getFamily_name());
            stmt.setObject(6, user.getEmail());
            stmt.setObject(7, user.getStatus());
            stmt.setObject(8, OBJ.writeValueAsString(user));
            result = stmt.executeUpdate();
        } catch (JsonProcessingException | SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (conx != null && !conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
    
    public long deleteUser(String appId, String username) {
        LOGGER.trace("DELETE USER " + username);

        Connection conx = null;
        long result = 0;
        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(DELETE_USER);
            stmt.setString(1, appId);
            stmt.setString(2, username);
            result = stmt.executeUpdate();
        } catch (SQLException | NamingException e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            return result;
        } finally {
            try {
                if (conx != null && !conx.isClosed()) {
                    conx.close();
                }
            } catch (SQLException e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
    
    public long updateUser(String appId, String target, UserRequest user) {

        LOGGER.trace("UPDATE USER: " + user.getUsername());
        int result = 0;
        Connection conx = null;

        try {
            conx = getConnection();
            PreparedStatement stmt = conx.prepareStatement(UPDATE_USER);
            stmt.setObject(1, target);
            stmt.setObject(2, user.getName());
            stmt.setObject(3, user.getFamily_name());
            stmt.setObject(4, user.getEmail());
            stmt.setObject(5, user.getStatus());
            stmt.setObject(6, OBJ.writeValueAsString(user));
            stmt.setObject(7, appId);
            stmt.setObject(8, user.getUsername());
            result = stmt.executeUpdate();
        } catch (Exception e) {
            LOGGER.error(e.getLocalizedMessage(), e);
            result = 0;
        } finally {
            try {
                if (!conx.isClosed()) {
                    conx.close();
                }
            } catch (Exception e) {
                LOGGER.error(e.getLocalizedMessage(), e);
            }
        }
        return result;
    }
}

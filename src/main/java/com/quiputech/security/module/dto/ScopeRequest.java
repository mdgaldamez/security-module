/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value="ScopeRequest", description="List of scopes")
public class ScopeRequest {
    private List<Scope> scopes;

    @ApiModelProperty(value = "List of scopes", required = true)
    public List<Scope> getScopes() {
        return scopes;
    }

    public void setScopes(List<Scope> scopes) {
        this.scopes = scopes;
    }
}

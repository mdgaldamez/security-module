package com.quiputech.security.module.dto;

import com.amazonaws.services.cognitoidp.model.AuthenticationResultType;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quiputech.security.module.entities.AccessTokenResponse;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Peter
 */
@ApiModel(value="SecurityResponse", description="Security Response with Keys")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecurityResponse {
    private String accessToken;
    private String refreshToken;
    private List<SecurityKey> keys;
    private int expiresIn;
    private List<String> groups;
    private Map<String, String> attributes;

    private SecurityResponse(String accessToken, String refreshToken, int expiresIn, List<SecurityKey> keys) {
        this.accessToken = accessToken;
        this.refreshToken = refreshToken;
        this.expiresIn = expiresIn;
        this.keys = keys;
    }
        
    @ApiModelProperty(value = "Access Token", required = true)
    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }

    @ApiModelProperty(value = "Refresh Token", required = true)
    public String getRefreshToken() {
        return refreshToken;
    }

    public void setRefreshToken(String refreshToken) {
        this.refreshToken = refreshToken;
    }    

    @ApiModelProperty(value = "Expires in seconds", required = true)
    public int getExpiresIn() {
        return expiresIn;
    }

    public void setExpiresIn(int expiresIn) {
        this.expiresIn = expiresIn;
    }

    @ApiModelProperty(value = "Encryption keys", required = true)
    public List<SecurityKey> getKeys() {
        if (keys == null) {
            keys = new ArrayList<>();
        }
        return keys;
    }

    public void setKeys(List<SecurityKey> keys) {
        this.keys = keys;
    }  
    
    @ApiModelProperty(value = "Groups", required = false)
    public List<String> getGroups() {
        return groups;
    }

    public void setGroups(List<String> groups) {
        this.groups = groups;
    }
    
    public Map<String, String> getAttributes() {
        if (attributes == null)
            attributes = new LinkedHashMap<>();
        return attributes;
    }

    public void setAttributes(Map<String, String> attributes) {
        this.attributes = attributes;
    }
    
    public static Builder builder(int expiresIn, String accessToken, String refreshToken) {
        return new Builder(expiresIn, accessToken, refreshToken);
    }
    
    public static Builder builder(AuthenticationResultType result) {
        return new Builder(result.getExpiresIn(), result.getAccessToken(), result.getRefreshToken());
    }
    
    public static Builder builder(AccessTokenResponse result) {
        return new Builder((int) result.getExpiresIn(), result.getToken(), result.getRefreshToken());
    }
    
    public static Builder builder() {
        return new Builder();
    }    
    
    public static final class Builder {
        private int expiresIn;
        private String accessToken;
        private String refreshToken;
        private List<SecurityKey> keys;
        
        public Builder() {
            
        }
        
        public Builder(int expiresIn, String accessToken, String refreshToken) {
            this.expiresIn = expiresIn;
            this.accessToken = accessToken;
            this.refreshToken = refreshToken;
            this.keys = new ArrayList<>();
        }

        public Builder withExpiresIn(int expiresIn) {
            this.expiresIn = expiresIn;
            return this;
        }
        
        public Builder withAccessToken(String accessToken) {
            this.accessToken = accessToken;
            return this;
        }
        
        public Builder withRefreshToken(String refreshToken) {
            this.refreshToken = refreshToken;
            return this;
        }
        
        public Builder withKeys(List<SecurityKey> keys) {
            this.keys = keys;
            return this;
        }
        
        public Builder withKey(SecurityKey key) {
            if (this.keys == null) {
                this.keys = new ArrayList<>();
            }
            this.keys.add(key);
            return this;
        }

        public SecurityResponse build() {            
            return new SecurityResponse(accessToken, refreshToken, expiresIn, keys);
        }
    }
        
}

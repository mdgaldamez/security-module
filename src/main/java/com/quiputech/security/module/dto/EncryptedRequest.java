/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value="EncryptedRequest", description="Request encrypted")
@JsonIgnoreProperties(ignoreUnknown = true)
public class EncryptedRequest {
    private String key;
    private String payload;
    

    @ApiModelProperty(value = "Encryption Key identifier", required = true, example = "2c4ffdad-ef3e-4ab0-976a-107dd0270cfd")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @ApiModelProperty(value = "Payload encrypted", required = true, example = "9c8ffdadef3e4ab0976b107dd0370cfd")
    public String getPayload() {
        return payload;
    }

    public void setPayload(String payload) {
        this.payload = payload;
    } 
}

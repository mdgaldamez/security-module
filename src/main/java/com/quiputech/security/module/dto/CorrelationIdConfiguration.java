/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import java.util.Optional;

public class CorrelationIdConfiguration {

    public final String headerName;
    public final String mdcKey;

    public CorrelationIdConfiguration() {
        this(null, null);
    }

    public CorrelationIdConfiguration(@JsonProperty("headerName") String headerName,
            @JsonProperty("mdcKey") String mdcKey) {
        this.headerName = Optional.ofNullable(headerName).orElse("X-Correlation-Id");
        this.mdcKey = Optional.ofNullable(mdcKey).orElse("correlationId");
    }

}

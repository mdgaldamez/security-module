/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

/**
 *
 * @author Peter
 */
public class UserResponse {

    private boolean valid;
    private String userName;
    private String clientIp;
    private String userAgent;
    private String correlationId;
    private String path;
    private String poolId;

    private UserResponse(boolean valid, String userName, String poolId, String clientIp, String userAgent, String path) {
        this.valid = valid;
        this.userName = userName;
        this.poolId = poolId;
        this.path = path;
        this.userAgent = userAgent;
        this.clientIp = clientIp;
    }

    public boolean isValid() {
        return valid;
    }

    public void setValid(boolean valid) {
        this.valid = valid;
    }

    public String getCorrelationId() {
        return correlationId;
    }

    public void setCorrelationId(String correlationId) {
        this.correlationId = correlationId;
    }
    
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getClientIp() {
        return clientIp;
    }

    public void setClientIp(String clientIp) {
        this.clientIp = clientIp;
    }

    public String getUserAgent() {
        return userAgent;
    }

    public void setUserAgent(String userAgent) {
        this.userAgent = userAgent;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getPoolId() {
        return poolId;
    }

    public void setPoolId(String poolId) {
        this.poolId = poolId;
    }
    
    public static Builder builder(boolean valid, String userName, String poolId) {
        return new Builder(userName, poolId, valid, null, null, null);
    }
    
    public static Builder builder(boolean valid, String userName, String poolId, String clientIp, String userAgent, String path) {
        return new Builder(userName, poolId, valid, clientIp, userAgent, path);
    }
    
    public static Builder builder() {
        return new Builder(null, null, false, null, null, null);
    }

    public static final class Builder {

        private boolean valid;
        private String userName;
        private String poolId;
        private String clientIp;
        private String userAgent;
        private String path;
        
        public Builder() {

        }

        public Builder(String userName, String poolId, boolean valid, String clientIp, String userAgent, String path) {
            this.userName = userName;
            this.poolId = poolId;
            this.valid = valid;
            this.path = path;
            this.userAgent = userAgent;
            this.clientIp = clientIp;
        }

        public Builder withUserName(String userName) {
            this.userName = userName;
            return this;
        }

        public Builder withPoolId(String poolId) {
            this.poolId = poolId;
            return this;
        }

        public UserResponse build() {
            return new UserResponse(valid, userName, poolId, clientIp, userAgent, path);
        }
    }
}

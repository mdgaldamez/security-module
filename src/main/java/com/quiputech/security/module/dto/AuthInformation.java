/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import com.quiputech.security.module.enums.AuthorizeType;

/**
 *
 * @author Peter
 */
public class AuthInformation  {
     private String appId;
     private String userName;
     private String path;
     private int identifierPathPosition;
     private String identifierList;
     private String remoteIps;
     private boolean pathIdentifier;
     private boolean validateMerchant;
     private int merchantPosition;
     private boolean active;
     
     private AuthorizeType authorizeType;
     
    public String getAppId() {
        if (appId == null)
            appId = "";
        return appId;
    }

    public void setAppId(String appId) {
        this.appId = appId;
    }

    public String getUserName() {
        if (userName == null)
            userName = "";
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getRemoteIps() {
        return remoteIps;
    }

    public void setRemoteIps(String remoteIps) {
        this.remoteIps = remoteIps;
    }

    public boolean isPathIdentifier() {
        return pathIdentifier;
    }

    public void setPathIdentifier(boolean pathIdentifier) {
        this.pathIdentifier = pathIdentifier;
    }

    public int getIdentifierPathPosition() {
        return identifierPathPosition;
    }

    public void setIdentifierPathPosition(int identifierPathPosition) {
        this.identifierPathPosition = identifierPathPosition;
    }

    public String getIdentifierList() {
        return identifierList;
    }

    public void setIdentifierList(String identifierList) {
        this.identifierList = identifierList;
    }

    public boolean isValidateMerchant() {
        return validateMerchant;
    }

    public void setValidateMerchant(boolean validateMerchant) {
        this.validateMerchant = validateMerchant;
    }

    public int getMerchantPosition() {
        return merchantPosition;
    }

    public void setMerchantPosition(int merchantPosition) {
        this.merchantPosition = merchantPosition;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public AuthorizeType getAuthorizeType() {
        return authorizeType;
    }

    public void setAuthorizeType(AuthorizeType authorizeType) {
        this.authorizeType = authorizeType;
    }
}
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.quiputech.security.module.enums.KeyType;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.UUID;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value="SecurityKey", description="Security Key")
@JsonIgnoreProperties(ignoreUnknown = true)
public class SecurityKey {
    private String token;
    private String baseKey;
    @JsonIgnore
    private String derived;
    @JsonIgnore
    private String salt;
    private String iv;
    @JsonIgnore
    private KeyType type;

    public SecurityKey(String base, String derived, String salt, String iv, KeyType type) {
        this.baseKey = base;
        this.derived = derived;
        this.salt = salt;
        this.iv = iv;
        this.type = type;
        this.token = UUID.randomUUID().toString();
    }
    
    @ApiModelProperty(value = "Token identifier - Encryption Key", required = true, example = "2c4ffdad-ef3e-4ab0-976a-107dd0270cfd")
    public String getToken() {
        if (token == null)
            token = UUID.randomUUID().toString();
        return token;
    }

    @ApiModelProperty(value = "Base Encryption Key", required = true, example = "0cafacf482883aaccc1fa13fba0d0d576fe57a647ef32f8b8e306848faee2919")
    public String getBaseKey() {
        return baseKey;
    }

    public void setBaseKey(String base) {
        this.baseKey = base;
    }

    @ApiModelProperty(value = "Derived Encryption Key", required = true, example = "4c18406b9d5e0f1b58471cb34be4f2fa")
    public String getDerived() {
        return derived;
    }

    public void setDerived(String derived) {
        this.derived = derived;
    }

    @ApiModelProperty(value = "Salt Generated", required = true, example = "5344f84b2901474e2ec322914e392990")
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    @ApiModelProperty(value = "IV Generated", required = true, example = "48dfd77e05e754b39fe8df3b7270d6bf")
    public String getIv() {
        return iv;
    }

    public void setIv(String iv) {
        this.iv = iv;
    }

    @ApiModelProperty(value = "Key type", required = true, example = "WRAPPED")
    public KeyType getType() {
        return type;
    }

    public void setType(KeyType type) {
        this.type = type;
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.module.dto;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import java.util.List;

/**
 *
 * @author Peter Galdamez
 */
@ApiModel(value="Scope", description="Scope detail")
@JsonIgnoreProperties(ignoreUnknown = true)
public class Scope {
    private String key;
    private List<String> elements;

    @ApiModelProperty(value = "Scope key identifier", required = true, example = "9c4ffdad-ef4e-3ab0-576a-107dd0170cfd")
    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    @ApiModelProperty(value = "Encrypted elements")
    public List<String> getElements() {
        return elements;
    }

    public void setElements(List<String> elements) {
        this.elements = elements;
    }
}

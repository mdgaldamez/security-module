/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.jayway.jsonpath.PathNotFoundException;
import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.crypto.CryptoUtil;
import com.quiputech.security.module.crypto.RSAUtil;
import com.quiputech.security.module.crypto.SaltedSecretKey;
import com.quiputech.security.module.dto.EncryptedRequest;
import com.quiputech.security.module.rsa.RSAPairKeys;
import com.quiputech.security.module.session.SecuritySession;
import com.quiputech.security.module.utils.ParseUtil;
import com.quiputech.security.module.utils.SecurityUtil;
import com.quiputech.security.module.utils.StringUtil;
import java.io.IOException;
import java.io.InputStream;
import java.security.PrivateKey;
import java.util.Base64;
import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Peter Galdamez
 */
@Priority(value = 4)
public class RSAEncryptionFilter implements ContainerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(RSAEncryptionFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            if (requestContext.getMethod().equalsIgnoreCase(SecurityConstants.OPTIONS)) {
                return;
            }

            if (requestContext.getUriInfo() != null && requestContext.getUriInfo().getAbsolutePath() != null) {
                if (SecurityUtil.isRequestBodyless(requestContext)) {
                    return;
                }

                String ewIV = requestContext.getHeaderString(SecurityConstants.CRYPTO_CONTEXT_EWIV);
                String ewSalt = requestContext.getHeaderString(SecurityConstants.CRYPTO_CONTEXT_EWSALT);
                String tokenId = requestContext.getHeaderString(SecurityConstants.CRYPTO_CONTEXT_KEY);

                if (ewIV != null && !ewIV.isEmpty()
                        && ewSalt != null && !ewSalt.isEmpty()) {
                    String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                    EncryptedRequest request = ParseUtil.fromJson(json, EncryptedRequest.class);
                    if (request != null) {
                        SecuritySession session = new SecuritySession();
                        RSAPairKeys keys = session.retrieveRSAKeys(tokenId);
                        if (keys != null) {
                            PrivateKey privateKey = RSAUtil.buildPrivate(keys.getPrivateKey());
                            String key = StringUtil.toHex(RSAUtil.decrypt(request.getKey(), privateKey).getBytes());
                            String salt = StringUtil.toHex(RSAUtil.decrypt(ewSalt, privateKey).getBytes());
                            String iv = StringUtil.toHex(Base64.getDecoder().decode(ewIV));
                            String ciphierText = request.getPayload();
                            SaltedSecretKey saltedKey = com.quiputech.security.module.crypto.CryptoUtil.create256DerivedKey(key, salt);
                            String derived = StringUtil.toHex(saltedKey.getEncoded());
                            String decrypted = CryptoUtil.decrypt(ciphierText, derived, iv, null);

                            if (decrypted != null && !decrypted.isEmpty()) {
                                json = decrypted;
                            } else {
                                notValid(requestContext);
                            }
                        }
                    }

                    InputStream in = IOUtils.toInputStream(json);
                    requestContext.setEntityStream(in);
                }
            } else {
                abort(requestContext);
            }
        } catch (PathNotFoundException ex) {
            notValidMessage(requestContext, ex.getLocalizedMessage());
        } catch (RuntimeException ex) {
            notValidMessage(requestContext, ex.getLocalizedMessage());
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            unexpected(requestContext);
        }
    }

    public void abort(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_ACCEPTABLE).build());
    }

    public void notValid(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_VALID).build());
    }

    public void notValidMessage(ContainerRequestContext requestContext, String error) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(error).build());
    }

    public void unexpected(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.UNEXPECTED).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.UNEXPECTED_ERROR).build());
    }
}

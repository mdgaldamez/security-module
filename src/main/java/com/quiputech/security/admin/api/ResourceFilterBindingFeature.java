/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.quiputech.security.module.annotations.Encrypted;
import com.quiputech.security.module.annotations.Idempotent;
import javax.annotation.Priority;
import javax.ws.rs.container.DynamicFeature;
import javax.ws.rs.container.ResourceInfo;
import javax.ws.rs.core.FeatureContext;
import javax.ws.rs.ext.Provider;
import com.quiputech.security.module.annotations.NoSecurity;
import com.quiputech.security.module.annotations.Logged;
import com.quiputech.security.module.annotations.RSAEncrypted;

/**
 *
 * @author peter
 */


@Provider
@Priority(value = 1)
public class ResourceFilterBindingFeature implements DynamicFeature {

  @Override
  public void configure(ResourceInfo resourceInfo, FeatureContext context) {
    if (!resourceInfo.getResourceMethod().isAnnotationPresent(NoSecurity.class)) {
        context.register(AuthenticationFilter.class);
    }
    
    if (resourceInfo.getResourceMethod().isAnnotationPresent(Idempotent.class)) {
        context.register(IdempotentFilter.class);
    }
    
    if (resourceInfo.getResourceMethod().isAnnotationPresent(Encrypted.class)) {
        context.register(EncryptionFilter.class);
    }
    
    if (resourceInfo.getResourceMethod().isAnnotationPresent(Logged.class)) {
        context.register(CustomLoggingFilter.class);
    }
    
    if (resourceInfo.getResourceMethod().isAnnotationPresent(RSAEncrypted.class)) {
        context.register(RSAEncryptionFilter.class);
    }
  }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.jayway.jsonpath.PathNotFoundException;
import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.utils.SecurityUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Base64;
import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pgaldamez
 */
@Priority(value = 4)
public class EncryptionFilter implements ContainerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(EncryptionFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            if (requestContext.getMethod().equalsIgnoreCase(SecurityConstants.OPTIONS)) {
                return;
            }

            if (requestContext.getUriInfo() != null && requestContext.getUriInfo().getAbsolutePath() != null) {
                if (SecurityUtil.isRequestBodyless(requestContext)) {
                    return;
                }

                String accessToken = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
                String scopeToken = requestContext.getHeaderString(SecurityConstants.SCOPE);
                
                if (scopeToken != null && !scopeToken.isEmpty()) {
                    byte[] base64decoded = Base64.getDecoder().decode(scopeToken);
                    scopeToken = new String(base64decoded, "UTF-8");
                }
                
                String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                String request = SecurityUtil.decrypt(accessToken, scopeToken, json);
                if (request != null && !request.isEmpty()) {
                    InputStream in = IOUtils.toInputStream(request);
                    requestContext.setEntityStream(in);
                } else {
                    notValid(requestContext);
                }

            } else {
                abort(requestContext);
            }
        } catch (PathNotFoundException ex) {
            notValidMessage(requestContext, ex.getLocalizedMessage());
        } catch (RuntimeException ex) {
            notValidMessage(requestContext, ex.getLocalizedMessage());
        }catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
            unexpected(requestContext);
        } 
    }

    public void abort(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_ACCEPTABLE).build());
    }

    public void notValid(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_VALID).build());
    }

    public void notValidMessage(ContainerRequestContext requestContext, String error) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(error).build());
    }
    
    public void unexpected(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.UNEXPECTED).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.UNEXPECTED_ERROR).build());
    }
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.utils.MaskedCardUtil;
import com.quiputech.security.module.utils.ParseUtil;
import com.quiputech.security.module.utils.SecurityUtil;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.container.ContainerResponseContext;
import javax.ws.rs.container.ContainerResponseFilter;
import javax.ws.rs.core.Context;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author Peter Galdamez
 */
@Priority(value = 5)
public class CustomLoggingFilter implements ContainerRequestFilter, ContainerResponseFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(CustomLoggingFilter.class);

    private static final String TEMPLATE = "{\n"
            + "  \"headers\": \"%s\",\n"
            + "  \"entity\": %s\n"
            + "}";

    @Context
    private HttpServletRequest servletRequest;
    
    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        try {
            long startDate = Calendar.getInstance().getTimeInMillis();
            StringBuilder requestHeaders = new StringBuilder();
            requestHeaders.append(requestContext.getHeaders());
            String request = String.format(TEMPLATE, requestHeaders, MaskedCardUtil.maskCard(getEntityBody(requestContext)));
            requestContext.setProperty(SecurityConstants.X_TIME_START, startDate);
            requestContext.setProperty(SecurityConstants.X_PAYLOAD_REQUEST, request);
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    @Override
    public void filter(ContainerRequestContext requestContext, ContainerResponseContext responseContext) throws IOException {
        try {
            String correlationId = requestContext.getHeaderString(SecurityConstants.X_CORRELATION_ID);
            if (correlationId != null && !correlationId.isEmpty()) {
                Object startDate_o = requestContext.getProperty(SecurityConstants.X_TIME_START);
                Object payload = requestContext.getProperty(SecurityConstants.X_PAYLOAD_REQUEST);
                if (startDate_o != null) {
                    long startDate = (long) startDate_o;
                    String request = payload != null ? (String) payload : null;
                    StringBuilder resposeHeaders = new StringBuilder();
                    resposeHeaders.append(responseContext.getHeaders());
                    String userName = requestContext.getSecurityContext() == null || requestContext.getSecurityContext().getUserPrincipal() == null ? "unknown"
                            : requestContext.getSecurityContext().getUserPrincipal().getName();
                    String clientIp = SecurityUtil.getClientIp(servletRequest);
                    String userAgent = requestContext.getHeaderString(HttpHeaders.USER_AGENT);
                    String path = requestContext.getUriInfo().getAbsolutePath().toString();
                    String res = String.format(TEMPLATE, resposeHeaders.toString(), ParseUtil.toJson(responseContext.getEntity()));
                    SecurityUtil.addApiLog(correlationId, responseContext.getStatus(), path, startDate, userName, clientIp, userAgent, request, res);
                }
            }
        } catch (Exception ex) {
            LOGGER.error(ex.getLocalizedMessage(), ex);
        }
    }

    private String getEntityBody(ContainerRequestContext requestContext) throws IOException {
        String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
        InputStream in = IOUtils.toInputStream(json);
        requestContext.setEntityStream(in);
        return json;
    }
}

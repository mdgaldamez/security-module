package com.quiputech.security.admin.api;

import com.quiputech.security.module.CognitoManager;
import com.quiputech.security.module.DatabaseManager;
import com.quiputech.security.module.RedHatManager;
import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.crypto.DerivedKeyUtil;
import com.quiputech.security.module.dto.SecurityKey;
import com.quiputech.security.module.dto.SecurityResponse;
import com.quiputech.security.module.dto.UserResponse;
import com.quiputech.security.module.entities.Settings;
import com.quiputech.security.module.enums.KeyType;
import com.quiputech.security.module.utils.SecurityUtil;
import java.security.Principal;
import java.util.Base64;
import javax.annotation.Priority;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.SecurityContext;
import javax.ws.rs.core.UriInfo;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Priority(value = 2)
public class AuthenticationFilter implements ContainerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);

    @Context
    private HttpServletRequest request;

    @Context
    UriInfo uriInfo;

    public AuthenticationFilter() {

    }

    public void abort(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_AUTHORIZATION_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NO_AUTHORIZATION).build());
    }

    public void forbidden(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_ALLOWED_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.FORBIDDEN_ACCESS).build());
    }
    
    public void notAllowed(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_ALLOWED_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_ALLOWED).build());
    }

    @Override
    public void filter(ContainerRequestContext requestContext) {

        if (requestContext.getMethod().equalsIgnoreCase(SecurityConstants.OPTIONS)) {
            return;
        }
        final String path = request.getPathInfo();        

        if (path != null) {
            String[] appId = path.split("/");
            String mode = "";
            DatabaseManager db = new DatabaseManager(); 
            Settings settings = db.getSettingsByClientId(appId[1]);

            if(settings.getMode() == 0)
            {
                mode = "cognito";
                CognitoManager.CLIENTID = settings.getClientId();
                CognitoManager.USERPOOLID = settings.getPoold();
                CognitoManager.REGION = settings.getRegion();
                CognitoManager.AWS_ISSUER_URL =  String.format(SecurityConstants.COGNITO_IDENTITY_POOL_URL, CognitoManager.REGION, CognitoManager.USERPOOLID);
                CognitoManager.AWS_JWK_URL = String.format(SecurityConstants.COGNITO_IDENTITY_POOL_URL + SecurityConstants.JSON_WEB_TOKEN_SET_URL_SUFFIX, CognitoManager.REGION, CognitoManager.USERPOOLID);
                
            }else {
                mode = "redHat";
                RedHatManager.CLIENT_ID = settings.getClientId();
                RedHatManager.CLIENT_SECRET = settings.getClientSecret();
                RedHatManager.USERNAME = settings.getUsername();
                RedHatManager.PASSWORD = settings.getPassword();
                RedHatManager.REALM = settings.getRealm();
            }
            try {
                String auth = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
                switch (path.replace("/"+appId[1], "")) {
                    case "/security":
                    case "/security/keys":
                    case "/security/accessToken":
                        if (auth == null || auth.isEmpty()) {
                            abort(requestContext);
                            return;
                        }
                        byte[] base64decoded = Base64.getDecoder().decode(auth.replace("Basic ", ""));
                        String authCredentials[] = new String(base64decoded, "UTF-8").split(":");
                        request.setAttribute("username", authCredentials[0]);
                        request.setAttribute("password", authCredentials[1]);
                        SecurityResponse result;
                        if(mode.equals("cognito"))
                        {
                            result = CognitoManager.createToken(authCredentials[0], authCredentials[1]);
                        } else {
                            result = RedHatManager.createToken(authCredentials[0], authCredentials[1]);
                        }
                        if (result != null) {
                            if (result.getAccessToken() != null && path.trim().toLowerCase().contains("keys")) {
                                SecurityKey wrapped = DerivedKeyUtil.createSecurityKey(KeyType.WRAPPED);
                                SecurityKey exposed = DerivedKeyUtil.createSecurityKey(KeyType.EXPOSED);
                                boolean created = SecurityUtil.createKeys(result.getAccessToken(), authCredentials[0], exposed, wrapped);
                                if (created) {
                                    result.getKeys().add(wrapped);
                                    result.getKeys().add(exposed);
                                }
                            } 

                            if (result.getAccessToken() != null && path.trim().toLowerCase().contains("accesstoken")) {
                                requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.TEXT_PLAIN).entity(result.getAccessToken()).build());
                            } else {
                                requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.APPLICATION_JSON).entity(result).build());
                            }
                        } else {
                            abort(requestContext);
                        }
                        break;                        
                    case "/security/refresh":
                    case "/security/refresh/keys":
                        SecurityResponse refresh = CognitoManager.attemptRefresh(auth);
                        if (refresh != null) {
                            refresh.setRefreshToken(auth);
                            if (refresh.getAccessToken() != null && path.trim().toLowerCase().contains("keys")) {
                                SecurityKey wrapped = DerivedKeyUtil.createSecurityKey(KeyType.WRAPPED);
                                SecurityKey exposed = DerivedKeyUtil.createSecurityKey(KeyType.EXPOSED);
                                boolean created = SecurityUtil.createKeys(refresh.getAccessToken(), SecurityUtil.sha256(refresh.getRefreshToken()), exposed, wrapped);
                                if (created) {
                                    refresh.getKeys().add(wrapped);
                                    refresh.getKeys().add(exposed);
                                }
                            }
                            
                            requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.APPLICATION_JSON).entity(refresh).build());
                        } else {
                            abort(requestContext);
                        }
                        break;
                    case "/security/encrypt":
                        if (auth != null) {
                            final UserResponse user = CognitoManager.validateToken(auth);
                            if (!user.isValid()) {
                                abort(requestContext);
                                return;
                            }

                            String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                            String encrypt = SecurityUtil.encrypt(auth, json);
                            if (encrypt != null && !encrypt.isEmpty() && !encrypt.equalsIgnoreCase(json)) {
                                requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.TEXT_PLAIN).entity(encrypt).build());
                            } else {
                                notAllowed(requestContext);
                            }
                        } else {
                            abort(requestContext);
                        }
                        break;
                    case "/security/decrypt":
                        if (auth != null) {
                            final UserResponse user = CognitoManager.validateToken(auth);
                            if (!user.isValid()) {
                                abort(requestContext);
                                return;
                            }

                            String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                            String decrypt = SecurityUtil.decrypt(auth, null, json);

                            if (decrypt != null && !decrypt.isEmpty() && !decrypt.equalsIgnoreCase(json)) {
                                requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.TEXT_PLAIN).entity(decrypt).build());
                            } else {
                                notAllowed(requestContext);
                            }
                        } else {
                            abort(requestContext);
                        }
                        break;
                    case "/security/base64/encode":
                        String encode = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                        if (encode != null && !encode.isEmpty()) {
                            String base64 = Base64.getEncoder().encodeToString(encode.getBytes());
                            requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.TEXT_PLAIN).entity(base64).build());
                        } else {
                            notAllowed(requestContext);
                        }
                        break;
                    case "/security/base64/decode":
                        String decode = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);
                        if (decode != null && !decode.isEmpty()) {
                            String base64 = new String(Base64.getDecoder().decode(decode),  "UTF-8");
                            requestContext.abortWith(Response.status(HttpServletResponse.SC_CREATED).type(MediaType.TEXT_PLAIN).entity(base64).build());
                        } else {
                            notAllowed(requestContext);
                        }
                        break;
                    case"/auth":
                        break;
                    default:
                        final UserResponse user = CognitoManager.validateToken(auth);
                        if (!user.isValid()) {
                            abort(requestContext);
                        } else {
                            final String ip = SecurityUtil.getClientIp(request);
                            user.setClientIp(ip);
                            user.setPath(requestContext.getUriInfo().getAbsolutePath().toString());
                            user.setUserAgent(requestContext.getHeaderString(HttpHeaders.USER_AGENT));
                            user.setCorrelationId(requestContext.getHeaderString(SecurityConstants.X_CORRELATION_ID));
                            requestContext.setSecurityContext(new SecurityContext() {
                                @Override
                                public Principal getUserPrincipal() {
                                    return () -> user.getUserName();
                                }

                                @Override
                                public boolean isUserInRole(String identifier) {
                                    return SecurityUtil.isUserInRole(user.getUserName(), uriInfo.getAbsolutePath().toString(), identifier, ip);
                                }

                                @Override
                                public boolean isSecure() {
                                    return uriInfo.getAbsolutePath().toString().toLowerCase().startsWith("https");
                                }

                                @Override
                                public String getAuthenticationScheme() {
                                    return "Token-Based-Auth-Scheme";
                                }
                            });
                        }
                        break;
                }
            } catch (Exception ex) {
                LOGGER.error(ex.getLocalizedMessage(), ex);
                requestContext.abortWith(Response.status(HttpServletResponse.SC_BAD_REQUEST).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.BAD_REQUEST_MESSAGE).build());
            }
        } else {
            abort(requestContext);
        }
    }

}

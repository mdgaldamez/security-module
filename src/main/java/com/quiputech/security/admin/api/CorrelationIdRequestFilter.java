/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.quiputech.security.module.dto.CorrelationIdConfiguration;
import java.io.IOException;
import java.util.Optional;
import java.util.UUID;
import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import org.slf4j.MDC;

@Provider
@Priority(value = 0)
public class CorrelationIdRequestFilter implements ContainerRequestFilter {

    private final CorrelationIdConfiguration configuration;

    public CorrelationIdRequestFilter() {
        this(new CorrelationIdConfiguration());
    }

    public CorrelationIdRequestFilter(CorrelationIdConfiguration configuration) {
        this.configuration = configuration;
    }

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        String correlationId = Optional.ofNullable(MDC.get(configuration.mdcKey)).orElseGet(() -> UUID.randomUUID().toString());
        requestContext.getHeaders().add(configuration.headerName, correlationId);
    }

}

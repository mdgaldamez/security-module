/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.quiputech.security.admin.api;

import com.quiputech.security.module.constants.SecurityConstants;
import com.quiputech.security.module.utils.SecurityUtil;
import java.io.IOException;
import java.io.InputStream;
import javax.annotation.Priority;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.apache.commons.io.Charsets;
import org.apache.commons.io.IOUtils;
import org.apache.http.HttpHeaders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author pgaldamez
 */
@Priority(value = 3)
public class IdempotentFilter implements ContainerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(IdempotentFilter.class);

    @Override
    public void filter(ContainerRequestContext requestContext) throws IOException {
        if (requestContext.getMethod().equalsIgnoreCase(SecurityConstants.OPTIONS)) {
            return;
        }

        if (requestContext.getUriInfo() != null && requestContext.getUriInfo().getAbsolutePath() != null) {

            if (SecurityUtil.isRequestBodyless(requestContext)) {
                return;
            }

            String accessToken = requestContext.getHeaderString(HttpHeaders.AUTHORIZATION);
            String endpoint = requestContext.getUriInfo().getAbsolutePath().toString();
            String json = IOUtils.toString(requestContext.getEntityStream(), Charsets.UTF_8);

            if (!SecurityUtil.isIdempotent(accessToken, endpoint, json)) {
                abort(requestContext);
            }

            InputStream in = IOUtils.toInputStream(json);
            requestContext.setEntityStream(in);
        } else {
            abort(requestContext);
        }
    }

    public void abort(ContainerRequestContext requestContext) {
        requestContext.abortWith(Response.status(SecurityConstants.NO_IDEMPOTENT_CODE).type(MediaType.TEXT_PLAIN).entity(SecurityConstants.NOT_ACCEPTABLE).build());
    }

}
